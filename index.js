import { Navigation } from "react-native-navigation";
import HomeScreen from "./src/screen/HomeScreen/HomeScreen";
import Category from "./src/screen/Category/Category";
import Cart from "./src/screen/Cart/Cart";
import ViewCart from "./src/screen/Cart/ViewCart";
import Account from "./src/screen/Account/Account";
// import Category from "./src/screen/ProductCategory/Category";
import ProductCategory from "./src/screen/ProductCategory/ProductCategory";
import Product from "./src/screen/Product/Product";
import JoinOrSignIn from "./src/screen/Join/JoinOrSignIn";
import Join from "./src/screen/Join/Join";
import SignIn from "./src/screen/Join/SignIn";
import ProductOption from "./src/screen/Product/ProductOption";
import DeliveryInformation from "./src/screen/OrderConfirm/DeliveryInformation";
import OrderConfirm from "./src/screen/OrderConfirm/OrderConfirm";
import OrderCompleted from "./src/screen/OrderConfirm/OrderCompleted";
import WishList from "./src/screen/Wishlist/WishList";
import Reviewed from "./src/screen/Reviewed/Reviewed";
import AddNewShippingAddress from "./src/screen/ShippingAddress/AddNewShippingAddress";
import UpdateShippingAddress from "./src/screen/ShippingAddress/UpdateShippingAddress";
import Shipped from "./src/screen/Shipped/Shipped";
import Search from "./src/screen/Search/Search";
import Promotion from "./src/screen/Promotion/Promotion";
import ShippingAddress from "./src/screen/ShippingAddress/ShippingAddress";
import Setting from "./src/screen/Setting/Setting";
import Profile from "./src/screen/Profile/Profile";
import UpdateDob from "./src/screen/Profile/UpdateDob";
import PrivacyPolicy from "./src/screen/PrivacyPolicy/PrivacyPolicy";
import store from "./src/class/redux/Store";
import { Provider } from "react-redux";

Navigation.registerComponentWithRedux(
  `HomeScreen`,
  () => HomeScreen,
  Provider,
  store
);
Navigation.registerComponent(
  `AddNewShippingAddress`,
  () => AddNewShippingAddress
);
Navigation.registerComponent(
  `UpdateShippingAddress`,
  () => UpdateShippingAddress
);
Navigation.registerComponent(`WishList`, () => WishList);
Navigation.registerComponent(`Reviewed`, () => Reviewed);
Navigation.registerComponent(`Shipped`, () => Shipped);
Navigation.registerComponentWithRedux(
  `Category`,
  () => Category,
  Provider,
  store
);
Navigation.registerComponentWithRedux(`Cart`, () => Cart, Provider, store);
Navigation.registerComponentWithRedux(
  `ViewCart`,
  () => ViewCart,
  Provider,
  store
);
Navigation.registerComponentWithRedux(
  `Account`,
  () => Account,
  Provider,
  store
);
// Navigation.registerComponent(`Category`, () => Category);
Navigation.registerComponent(`ProductCategory`, () => ProductCategory);
Navigation.registerComponent(`Search`, () => Search);
Navigation.registerComponent(`Promotion`, () => Promotion);
Navigation.registerComponent(`ShippingAddress`, () => ShippingAddress);
Navigation.registerComponent(`Setting`, () => Setting);
Navigation.registerComponent(`Profile`, () => Profile);
Navigation.registerComponent(`UpdateDob`, () => UpdateDob);
Navigation.registerComponent(`PrivacyPolicy`, () => PrivacyPolicy);
Navigation.registerComponentWithRedux(
  `Product`,
  () => Product,
  Provider,
  store
);
Navigation.registerComponentWithRedux(
  `JoinOrSignIn`,
  () => JoinOrSignIn,
  Provider,
  store
);
Navigation.registerComponentWithRedux(`Join`, () => Join, Provider, store);
Navigation.registerComponentWithRedux(
  `ProductOption`,
  () => ProductOption,
  Provider,
  store
);
Navigation.registerComponentWithRedux(`SignIn`, () => SignIn, Provider, store);
Navigation.registerComponent(`DeliveryInformation`, () => DeliveryInformation);
Navigation.registerComponent(`OrderConfirm`, () => OrderConfirm);
Navigation.registerComponent(`OrderCompleted`, () => OrderCompleted);
Navigation.setRoot({
  root: {
    bottomTabs: {
      options: {
        bottomTabs: {
          animate: false,
          titleDisplayMode: "alwaysHide"
        }
      },
      children: [
        {
          stack: {
            children: [
              {
                component: {
                  name: "HomeScreen"
                }
              }
            ],
            options: {
              bottomTab: {
                icon: require("./src/assets/icon/home.png"),
                text: "HOME"
              }
            }
          }
        },
        {
          stack: {
            children: [
              {
                component: {
                  name: "Category"
                }
              }
            ],
            options: {
              bottomTab: {
                icon: require("./src/assets/icon/power.png"),
                text: "DEAL"
              }
            }
          }
        },
        {
          stack: {
            children: [
              {
                component: {
                  name: "Cart"
                }
              }
            ],
            options: {
              bottomTab: {
                icon: require("./src/assets/icon/cart.png"),
                text: "CART"
              }
            }
          }
        },
        {
          stack: {
            children: [
              {
                component: {
                  name: "Account"
                }
              }
            ],
            options: {
              bottomTab: {
                icon: require("./src/assets/icon/account.png"),
                text: "ACCOOUNT"
              }
            }
          }
        }
      ]
    }
  }
});
