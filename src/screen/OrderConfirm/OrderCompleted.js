import React, { Component } from "react";

import {
  Container,
  Content,
  Text,
  Button,
  Footer,
  FooterTab
} from "native-base";

import { StyleSheet, Image, View, Dimensions } from "react-native";
import { Navigation } from "react-native-navigation";

const { width: screenWidth } = Dimensions.get("window");
const { height: screenHeight } = Dimensions.get("window");

class OrderCompleted extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  goToHome = () => {
    Navigation.setRoot({
      root: {
        bottomTabs: {
          options: {
            bottomTabs: {
              animate: false,
              titleDisplayMode: "alwaysHide"
            }
          },
          children: [
            {
              stack: {
                children: [
                  {
                    component: {
                      name: "HomeScreen"
                    }
                  }
                ],
                options: {
                  bottomTab: {
                    text: "Home",
                    icon: require("../../assets/icon/home.png")
                  }
                }
              }
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: "Deal"
                    }
                  }
                ],
                options: {
                  bottomTab: {
                    text: "Deal",
                    icon: require("../../assets/icon/power.png")
                  }
                }
              }
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: "Cart"
                    }
                  }
                ],
                options: {
                  bottomTab: {
                    text: "Cart",
                    icon: require("../../assets/icon/cart.png")
                  }
                }
              }
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: "Account"
                    }
                  }
                ],
                options: {
                  bottomTab: {
                    text: "Account",
                    icon: require("../../assets/icon/account.png")
                  }
                }
              }
            }
          ]
        }
      }
    });
  };
  render() {
    return (
      <Container style={style.container}>
        <Content>
          <View
            style={{
              width: screenWidth,
              height: screenHeight,
              justifyContent: "center",
              alignContent: "center"
            }}
          >
            <Image
              style={{
                alignSelf: "center"
              }}
              source={require("../../assets/icon/success.png")}
            ></Image>
            <Text style={style.textThankYou}>Thank You !</Text>
          </View>
        </Content>
        <Footer>
          <FooterTab>
            <Button style={style.buy} onPress={() => this.goToHome()}>
              <Text style={style.buyText}>SHOP NOW</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default OrderCompleted;

const style = StyleSheet.create({
  container: {
    backgroundColor: "#f7f7f7"
  },
  textThankYou: {
    fontSize: 18,
    fontWeight: "500",
    color: "black",
    alignSelf: "center"
  },
  buy: {
    backgroundColor: "#ff4747",
    width: "100%",
    borderRadius: 5
  },
  buyText: {
    fontSize: 15,
    fontWeight: "500",
    color: "white"
  }
});
