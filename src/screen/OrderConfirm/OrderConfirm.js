import React, { Component } from "react";

import {
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
  Title,
  Text,
  Card,
  CardItem,
  Button,
  Icon,
  Footer,
  FooterTab,
  List,
  ListItem
} from "native-base";

import { Grid, Col } from "react-native-easy-grid";

import {
  StyleSheet,
  Image,
  TouchableOpacity,
  View,
  Dimensions
} from "react-native";

import { siteUrl, baseUrl } from "../../class/Constant";

import { Navigation } from "react-native-navigation";

import ServerClass from "../../class/ServerClass";

const { width: screenWidth } = Dimensions.get("window");
const { height: screenHeight } = Dimensions.get("window");

class OrderConfirm extends Component {
  mounted = false;
  constructor(props) {
    super(props);

    this.state = {
      cart: [],
      cartDetail: [],
      user: 0,
      qty: 0,
      totalAmount: 0.0,
      shippingAddress: []
    };
  }

  componentDidMount = () => {
    this.mounted = true;
    this.responseShippingAddress();
  };

  componentWillUnmount() {
    this.mounted = false;
  }

  responseCartItem = () => {
    if (this.props.user > 0) {
      const params = {
        user_id: this.props.user
      };

      ServerClass.responsePostData(
        siteUrl + "mobileApi/responseCartItem",
        params
      ).then(json => {
        var cart = [];
        var cartDetail = [];
        var totalAmount = 0;
        if (this.mounted) {
          for (i = 0; i < json.Cart.length; i++) {
            totalAmount = json.Cart[i].cart_amount;
            cart.push({
              cart_id: json.Cart[i].cart_id,
              cart_amount: json.Cart[i].cart_amount,
              cart_status: json.Cart[i].cart_status
            });

            for (j = 0; j < json.Cart[i].Detail.length; j++) {
              cartDetail.push({
                cart_detail_id: json.Cart[i].Detail[j].cart_detail_id,
                product_name_en: json.Cart[i].Detail[j].product_name_en,
                product_photo: json.Cart[i].Detail[j].product_photo,
                product_detail_id: json.Cart[i].Detail[j].product_detail_id,
                product_min_qty: json.Cart[i].Detail[j].product_min_qty,
                qty: json.Cart[i].Detail[j].qty,
                price: json.Cart[i].Detail[j].price,
                sub_total: json.Cart[i].Detail[j].sub_total,
                checked: false
              });
            }
          }

          this.setState({
            cart: cart,
            cartDetail: cartDetail,
            totalAmount: totalAmount
          });
        }
      });
    }
  };

  responseShippingAddress = () => {
    if (this.mounted) {
      const params = {
        user: this.props.user
      };

      ServerClass.responsePostData(
        siteUrl + "mobileApi/responseShippingAddress",
        params
      ).then(json => {
        this.setState({ shippingAddress: json });
        this.responseCartItem();
      });
    }
  };

  savePayment = () => {
    const params = {
      user_id: this.props.user,
      totalAmount: this.state.totalAmount,
      cartDetail: this.props.checkArr
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/savePayment",
      params
    ).then(json => {
      if (json.statusCode == "S0001") {
        Navigation.push(this.props.componentId, {
          component: {
            name: "OrderCompleted",
            options: {
              topBar: {
                visible: false,
                drawBehind: true,
                animate: false
              },
              bottomTabs: {
                visible: false,
                drawBehind: true,
                animate: false
              },
              animations: {
                setStackRoot: {
                  enabled: true
                }
              }
            }
          }
        });
      }
    });
  };

  goToScreen = screen => {
    Navigation.push(this.props.componentId, {
      component: {
        name: screen,
        passProps: {
          user: this.props.user
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };

  backScreen = () => {
    Navigation.pop(this.props.componentId);
  };
  render() {
    return (
      <Container>
        <Header noShadow style={{ backgroundColor: "#ff4747" }}>
          <Left>
            <Button transparent onPress={() => this.backScreen()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>

          <Body>
            <Title>Order Confirmation</Title>
          </Body>
        </Header>

        <Content padder>
          <Card>
            <CardItem header>
              <Body>
                {this.state.shippingAddress.map(item => {
                  return (
                    <View key={item.shipping_setting_id}>
                      <Text>{item.user_name}</Text>
                      <Text>{item.shipping_address}</Text>
                      <Text>{item.shipping_state}</Text>
                      <Text>
                        {item.shipping_city},{item.shipping_country}
                      </Text>
                      <Text>{item.shipping_phone}</Text>
                    </View>
                  );
                })}
              </Body>
              <Right>
                <TouchableOpacity
                  onPress={() => this.goToScreen("ShippingAddress")}
                >
                  <Text style={{ color: "red" }}>Change</Text>
                </TouchableOpacity>
              </Right>
            </CardItem>
          </Card>

          {this.props.checkArr.map((item, index) => {
            return (
              <Card key={item.cart_detail_id}>
                <CardItem>
                  <Left>
                    <Image
                      source={{
                        uri: baseUrl + "images/product/" + item.product_photo
                      }}
                      style={style.itemImage}
                    />
                  </Left>
                  <Body>
                    <List>
                      <ListItem noBorder>
                        <Text>
                          {item.product_name_en.slice(0, 50) +
                            (item.product_name_en.length > 50 ? "..." : "")}
                        </Text>
                      </ListItem>
                      <ListItem noBorder>
                        <Text style={style.itemPrice}>US ${item.price}</Text>
                      </ListItem>
                    </List>
                  </Body>
                </CardItem>
              </Card>
            );
          })}
        </Content>
        <Footer style={{ backgroundColor: "white" }}>
          <FooterTab style={{ backgroundColor: "white" }}>
            <Grid>
              <Col style={{ alignItems: "flex-end" }}>
                <View
                  style={{
                    padding: 10,
                    alignItems: "center",
                    justifyContent: "center",
                    width: "100%",
                    height: "100%"
                  }}
                >
                  <Title style={style.totalAmount}>
                    Total US ${this.props.totalAmount}
                  </Title>
                </View>
              </Col>
              <Col style={{ alignItems: "flex-end" }}>
                <View
                  style={{
                    padding: 10,
                    alignItems: "center",
                    justifyContent: "center",
                    width: "100%",
                    height: "100%"
                  }}
                >
                  <Button style={style.buy} onPress={() => this.savePayment()}>
                    <Text style={style.buyText}>PLACE ORDER</Text>
                  </Button>
                </View>
              </Col>
            </Grid>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default OrderConfirm;

const style = StyleSheet.create({
  container: {
    backgroundColor: "#f7f7f7"
  },
  top: {
    width: screenWidth,
    backgroundColor: "#ff4747"
  },

  itemPrice: {
    fontWeight: "500",
    fontSize: 18,
    color: "black",
    fontStyle: "italic"
  },

  afterDiscount: {
    fontSize: 15,
    color: "#808080",
    textDecorationLine: "line-through",
    marginRight: 5
  },
  textInput: {
    width: "100%",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "grey",
    fontSize: 18
  },
  picker: {
    height: 50,
    width: "100%"
  },
  spinnerView: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  spinnerBackground: {
    height: screenHeight,
    width: screenWidth,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,0.2)"
  },
  itemImage: {
    borderColor: "#ddd",
    borderWidth: 1,
    height: 128,
    width: 128,
    aspectRatio: 1,
    resizeMode: "stretch",
    borderRadius: 8,
    padding: 10,
    marginRight: 5
  },
  totalAmount: {
    fontWeight: "500",
    fontSize: 18,
    color: "black"
  },
  buy: {
    backgroundColor: "#ff4747",
    width: "100%",
    borderRadius: 5
  },
  buyText: {
    fontSize: 15,
    fontWeight: "500",
    color: "white"
  }
});
