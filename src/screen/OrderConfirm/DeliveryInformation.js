import React, { Component } from "react";

import {
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
  Title,
  Text,
  Card,
  CardItem,
  Button,
  Item,
  Input,
  Icon,
  Picker,
  Textarea,
  Form
} from "native-base";

import {
  StyleSheet,
  View,
  ActivityIndicator,
  Dimensions,
  ToastAndroid
} from "react-native";

import { siteUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

import { Navigation } from "react-native-navigation";

const { width: screenWidth } = Dimensions.get("window");
const { height: screenHeight } = Dimensions.get("window");

class DeliveryInformation extends Component {
  mounted = false;
  constructor(props) {
    super(props);
    this.state = {
      country: [],
      countryCode: "KH",
      email: "",
      phone: "",
      province: "",
      city: "",
      address: "",
      note: "",
      isFalse: false,
      isLoading: false
    };
  }

  componentDidMount() {
    this.mounted = true;
    this.responseCountry();
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  onContinue = () => {
    var isFalse = false;
    if (this.state.email == "" || this.state.email != "") {
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (reg.test(this.state.email) === false) {
        ToastAndroid.show("Your email is not valid !", ToastAndroid.SHORT);
      } else if (this.state.phone == "") {
        ToastAndroid.show(
          "Please enter your Phone Number !",
          ToastAndroid.SHORT
        );
      } else if (this.state.province == "") {
        ToastAndroid.show(
          "Please enter your State/Province/County !",
          ToastAndroid.SHORT
        );
      } else if (this.state.city == "") {
        ToastAndroid.show("Please enter your City !", ToastAndroid.SHORT);
      } else if (this.state.address == "") {
        ToastAndroid.show("Please enter your Address !", ToastAndroid.SHORT);
      } else {
        this.setState({ isLoading: true });
        isFalse = true;
      }
    }

    if (isFalse) {
      const params = {
        email: this.state.email,
        address: this.state.address,
        phone: this.state.phone,
        province: this.state.province,
        city: this.state.city,
        countryCode: this.state.countryCode,
        note: this.state.note,
        user_id: this.props.user
      };

      ServerClass.responsePostData(
        siteUrl + "mobileApi/saveShipping",
        params
      ).then(json => {
        if (json.statusCode == "S0001") {
          Navigation.showModal({
            stack: {
              children: [
                {
                  component: {
                    name: "OrderConfirm",
                    passProps: {
                      user: this.props.user,
                      checkArr: this.props.checkArr,
                      totalAmount: this.props.totalAmount
                    },
                    options: {
                      topBar: {
                        visible: false,
                        drawBehind: true,
                        animate: false
                      },
                      bottomTabs: {
                        visible: false,
                        drawBehind: true,
                        animate: false
                      },
                      animations: {
                        setStackRoot: {
                          enabled: true
                        }
                      }
                    }
                  }
                }
              ]
            }
          });
        }
      });
    }
  };

  responseCountry = () => {
    if (this.mounted) {
      ServerClass.responsGetData(siteUrl + "mobileApi/responseCountry").then(
        json => {
          this.setState({ country: json });
        }
      );
    }
  };

  backScreen = () => {
    Navigation.pop(this.props.componentId);
  };

  render() {
    return (
      <Container>
        <Header noShadow style={{ backgroundColor: "#ff4747" }}>
          <Left>
            <Button transparent onPress={() => this.backScreen()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>

          <Body>
            <Title>Almost There !</Title>
          </Body>

          <Right></Right>
        </Header>

        <Content padder>
          <Item regular style={{ marginTop: 10 }}>
            <Input
              placeholder="Enter email address"
              placeholderTextColor="#DDDDDD"
              onChangeText={val => this.setState({ email: val })}
            />
          </Item>

          <Item picker style={{ marginTop: 10 }}>
            <Picker
              renderHeader={backAction => (
                <Header style={{ backgroundColor: "#ff4747" }}>
                  <Left>
                    <Button transparent onPress={backAction}>
                      <Icon name="arrow-back" style={{ color: "#fff" }} />
                    </Button>
                  </Left>
                  <Body style={{ flex: 3 }}>
                    <Title style={{ color: "#fff" }}>Choose your country</Title>
                  </Body>
                  <Right />
                </Header>
              )}
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              selectedValue={this.state.countryCode}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ countryCode: itemValue })
              }
            >
              {this.state.country.map(val => {
                return (
                  <Picker.Item
                    key={"country-item-" + val.country_id}
                    label={val.country_name}
                    value={val.iso2}
                  />
                );
              })}
            </Picker>
          </Item>

          <Item regular style={{ marginTop: 10 }}>
            <Input
              placeholder="Phone number e.g +(855) 969822229"
              placeholderTextColor="#DDDDDD"
              onChangeText={val => this.setState({ phone: val })}
            />
          </Item>

          <Item regular style={{ marginTop: 10 }}>
            <Input
              placeholder="State/Province/County"
              placeholderTextColor="#DDDDDD"
              onChangeText={val => this.setState({ province: val })}
            />
          </Item>

          <Item regular style={{ marginTop: 10 }}>
            <Input
              placeholder="City"
              placeholderTextColor="#DDDDDD"
              onChangeText={val => this.setState({ city: val })}
            />
          </Item>
          <Textarea
            rowSpan={5}
            bordered
            placeholder="Shipping Address"
            placeholderTextColor="#DDDDDD"
            style={{ marginTop: 10 }}
            onChangeText={val => this.setState({ address: val })}
          />
          <Textarea
            rowSpan={5}
            bordered
            placeholder="Note about your order e.g special notes for delivery"
            placeholderTextColor="#DDDDDD"
            style={{ marginTop: 10 }}
            onChangeText={val => this.setState({ note: val })}
          />

          <Button
            block
            danger
            style={{ marginTop: 10 }}
            onPress={() => this.onContinue()}
          >
            <Text>Continue</Text>
          </Button>

          {this.state.isLoading ? (
            <View style={style.spinnerBackground}>
              <View style={style.spinnerView}>
                <ActivityIndicator size="large" color="red" />
              </View>
            </View>
          ) : null}
        </Content>
      </Container>
    );
  }
}

export default DeliveryInformation;

const style = StyleSheet.create({
  container: {
    backgroundColor: "#f7f7f7"
  },
  top: {
    width: screenWidth,
    backgroundColor: "#ff4747"
  },

  itemPrice: {
    fontWeight: "500",
    fontSize: 18,
    color: "black",
    fontStyle: "italic"
  },

  afterDiscount: {
    fontSize: 15,
    color: "#808080",
    textDecorationLine: "line-through",
    marginRight: 5
  },
  textInput: {
    width: "100%",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "grey",
    fontSize: 18
  },
  picker: {
    height: 50,
    width: "100%"
  },
  spinnerView: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  spinnerBackground: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.5)"
  }
});
