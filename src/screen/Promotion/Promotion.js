import React, { Component } from "react";
import { siteUrl, baseUrl } from "../../class/Constant";
import ServerClass from "../../class/ServerClass";
import {
  Container,
  Header,
  Content,
  Left,
  Icon,
  Card,
  CardItem,
  Item,
  Input
} from "native-base";

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  ActivityIndicator
} from "react-native";
import { Navigation } from "react-native-navigation";

class Promotion extends Component {
  mounted = false;
  static options() {
    return {
      topBar: {
        drawBehind: true,
        visible: false,
        animate: false
      }
    };
  }
  constructor(props) {
    super(props);

    this.state = {
      hotDeal: [],
      lastID: 0,
      isRefresh: false,
      isLoading: false
    };
  }

  responseDealProduct = () => {
    this.setState({ isRefresh: true });
    if (this.mounted) {
      this.setState({ isLoading: true });
      const params = {
        offset: this.state.lastID
      };
      ServerClass.responsePostData(
        siteUrl + "mobileApi/responseAlldeal",
        params
      ).then(json => {
        this.setState({
          hotDeal: this.state.hotDeal.concat(json),
          isRefresh: false,
          isLoading: false
        });
      });
    }
  };
  componentDidMount() {
    this.mounted = true;
    this.responseDealProduct();
  }

  componentWillUnmount() {
    this.mounted = false;
  }
  renderLoading = () => {
    if (!this.state.isLoading) return null;
    return <ActivityIndicator size="large" color="red" />;
  };
  goToScreen = id => {
    if (id != 0) {
      Navigation.push(this.props.componentId, {
        component: {
          name: "Product",
          passProps: {
            data: id,
            user: this.props.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    } else {
      Navigation.pop(this.props.componentId);
    }
  };
  goToSearch = text => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "Search",
              passProps: {
                type: text
              },
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                bottomTabs: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                animations: {
                  setStackRoot: {
                    enabled: true
                  }
                }
              }
            }
          }
        ]
      }
    });
  };
  handleLoadMore = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };
  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header
          searchBar
          rounded
          noShadow
          style={{ backgroundColor: "#ff4747" }}
        >
          <Icon
            name="arrow-back"
            style={{ alignSelf: "center", paddingRight: 10, color: "white" }}
            onPress={() => this.goToScreen(0)}
          />
          <Item>
            <Icon name="search" />
            <Input
              placeholder="I'm looking for..."
              onSubmitEditing={event => this.goToSearch(event.nativeEvent.text)}
            />
          </Item>
        </Header>

        <Content
          onScroll={({ nativeEvent }) => {
            if (this.handleLoadMore(nativeEvent)) {
              this.setState(
                { lastID: this.state.lastID + 20 },
                this.responseDealProduct
              );
            }
          }}
          style={{
            paddingLeft: 13,
            paddingRight: 13,
            backgroundColor: "#f2f2f2"
          }}
        >
          <FlatList
            data={this.state.hotDeal}
            renderItem={({ item }) => (
              <View style={{ flex: 1, flexDirection: "column" }}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => this.goToScreen(item.product_id)}
                >
                  <Card padder style={{ borderRadius: 10 }}>
                    <CardItem
                      cardBody
                      style={{ alignSelf: "center", borderRadius: 10 }}
                    >
                      <Image
                        source={{
                          uri: baseUrl + "images/product/" + item.product_photo
                        }}
                        style={style.itemImage}
                      />
                    </CardItem>

                    <CardItem>
                      <Left>
                        <Text>
                          {item.product_name.slice(0, 20) +
                            (item.product_name.length > 20 ? "..." : "")}
                        </Text>
                      </Left>
                    </CardItem>
                    <CardItem
                      footer
                      style={{
                        borderBottomLeftRadius: 10,
                        borderBottomRightRadius: 10
                      }}
                    >
                      <Left>
                        <Text style={style.itemPrice}>
                          US ${item.promotion_price}
                        </Text>
                      </Left>
                    </CardItem>
                    <CardItem>
                      <Left>
                        <Text
                          style={{
                            textDecorationLine: "line-through",
                            textDecorationStyle: "solid"
                          }}
                        >
                          US ${item.product_price}
                        </Text>
                        <Text> - </Text>
                        <Text>{item.promotion_discount}% off</Text>
                      </Left>
                    </CardItem>
                  </Card>
                </TouchableOpacity>
              </View>
            )}
            keyExtractor={item => item.product_id}
            numColumns={2}
            ListFooterComponent={this.renderLoading.bind(this)}
          />
        </Content>
      </Container>
    );
  }
}

export default Promotion;

const style = StyleSheet.create({
  itemImage: {
    flex: 1,
    width: "100%",
    height: undefined,
    aspectRatio: 1,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8
  },
  itemPrice: {
    fontWeight: "500",
    fontSize: 18,
    color: "black",
    fontStyle: "italic"
  }
});
