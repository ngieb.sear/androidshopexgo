import React, { Component } from "react";

import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Text,
  Button,
  Icon,
  Item,
  Input
} from "native-base";

import { StyleSheet, FlatList, Dimensions } from "react-native";

import { Navigation } from "react-native-navigation";

import { siteUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

const { width: screenWidth } = Dimensions.get("window");

class Category extends Component {
  mounted = false;
  constructor(props) {
    super(props);

    this.state = {
      typeData: [],
      selected: 0,
      lastID: 0,
      refresh: false
    };

    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount = () => {
    this.mounted = true;
    this.responseProductSubCategory();
  };

  componentWillUnmount() {
    this.mounted = false;
    if (this.navigationEventListener) {
      this.navigationEventListener.remove();
    }
  }

  onBack() {
    Navigation.pop(this.props.componentId);
  }

  responseProductSubCategory = () => {
    const params = {
      catId: this.props.id
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseProductSubCategory",
      params
    ).then(json => {
      if (this.mounted) {
        this.setState({
          typeData: json
        });
      }
    });
  };

  goToScreen = id => {
    if (id != 0) {
      Navigation.push(this.props.componentId, {
        component: {
          name: "ProductCategory",
          passProps: {
            type: id,
            user: this.props.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    }
  };

  searchModal = () => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "Search",
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                bottomTabs: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                animations: {
                  setStackRoot: {
                    enabled: true
                  }
                }
              }
            }
          }
        ]
      }
    });
  };

  render() {
    return (
      <Container style={style.container}>
        <Header
          searchBar
          rounded
          noShadow
          style={{ backgroundColor: "#ff4747" }}
        >
          <Item>
            <Button transparent onPress={() => this.onBack()}>
              <Icon name="arrow-back" />
            </Button>
            <Input
              placeholder="I'm looking for..."
              onTouchStart={() => this.searchModal()}
            />
          </Item>
          <Button transparent>
            <Text>Search</Text>
          </Button>
        </Header>
        <Content>
          <List>
            <FlatList
              data={this.state.typeData}
              renderItem={({ item }) => (
                <ListItem
                  onPress={() => this.goToScreen(item.product_sub_category_id)}
                >
                  <Text>{item.product_sub_category_name}</Text>
                </ListItem>
              )}
              keyExtractor={(item, index) => item.product_sub_category_id}
              numColumns={1}
            />
          </List>
        </Content>
      </Container>
    );
  }
}

export default Category;

const style = StyleSheet.create({
  container: {
    backgroundColor: "#f7f7f7"
  },
  top: {
    width: screenWidth,
    backgroundColor: "#ff4747"
  },

  itemPrice: {
    fontWeight: "500",
    fontSize: 18,
    color: "black",
    fontStyle: "italic"
  },

  afterDiscount: {
    fontSize: 15,
    color: "#808080",
    textDecorationLine: "line-through",
    marginRight: 5
  }
});
