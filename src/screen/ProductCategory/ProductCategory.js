import React, { Component } from "react";
import { siteUrl, sellerUrl } from "../../class/Constant";
import ServerClass from "../../class/ServerClass";
import { Navigation } from "react-native-navigation";
import {
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
  View,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Text,
  Content,
  Header,
  Left,
  Button,
  Icon,
  Item,
  Input,
  Card,
  CardItem,
  Picker,
  Form
} from "native-base";

const { width: screenWidth } = Dimensions.get("window");

class ProductCategory extends Component {
  mounted = false;

  constructor(props) {
    super(props);
    this.state = {
      productData: [],
      lastID: 0,
      selected: 0,
      isLoading: false
    };

    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    this.mounted = true;
    this.responseProductByType();
  }

  componentWillUnmount() {
    this.mounted = false;
    if (this.navigationEventListener) {
      this.navigationEventListener.remove();
    }
  }
  responseProductByType = () => {
    if (this.mounted) {
      this.setState({ isLoading: true });
      const params = {
        offset: this.state.lastID,
        type: this.props.type,
        sorted: this.state.selected
      };
      ServerClass.responsePostData(
        siteUrl + "mobileApi/responseProductByType",
        params
      ).then(json => {
        this.setState({
          productData: this.state.productData.concat(json),
          isLoading: false
        });
      });
    }
  };

  onSelectChanged(value, index) {
    this.setState({ selected: value, lastID: 0, isLoading: true });
    const params = {
      offset: 0,
      type: this.props.type,
      sorted: value
    };
    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseProductByType",
      params
    ).then(json => {
      this.setState({
        productData: json,
        isLoading: false
      });
    });
  }

  renderLoading = () => {
    if (!this.state.isLoading) return null;
    return <ActivityIndicator size="large" color="red" />;
  };

  goToScreen = id => {
    if (id != 0) {
      Navigation.push(this.props.componentId, {
        component: {
          name: "Product",
          passProps: {
            data: id,
            user: this.props.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    } else {
      Navigation.pop(this.props.componentId);
    }
  };

  searchModal = () => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "Search",
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                bottomTabs: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                animations: {
                  setStackRoot: {
                    enabled: true
                  }
                }
              }
            }
          }
        ]
      }
    });
  };

  handleLoadMore = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };
  render() {
    return (
      <Container style={style.container}>
        <Header
          searchBar
          rounded
          noShadow
          style={{ backgroundColor: "#ff4747" }}
        >
          <Item>
            <Button transparent onPress={() => this.goToScreen(0)}>
              <Icon name="arrow-back" />
            </Button>
            <Input
              placeholder="I'm looking for..."
              onTouchStart={() => this.searchModal()}
            />
          </Item>
          <Button transparent>
            <Text>Search</Text>
          </Button>
        </Header>

        <Content
          onScroll={({ nativeEvent }) => {
            if (this.handleLoadMore(nativeEvent)) {
              this.setState(
                { lastID: this.state.lastID + 20 },
                this.responseProductByType
              );
            }
          }}
        >
          <Form>
            <Picker
              note
              mode="dropdown"
              style={{ width: "100%" }}
              selectedValue={this.state.selected}
              onValueChange={this.onSelectChanged.bind(this)}
            >
              <Picker.Item label="--Filter--" value="0" />
              <Picker.Item label="Price (Low to High)" value="1" />
              <Picker.Item label="Price (High to Low)" value="2" />
            </Picker>
          </Form>
          <View style={{ paddingLeft: 13, paddingRight: 13, marginTop: 10 }}>
            <FlatList
              data={this.state.productData}
              renderItem={({ item }) => (
                <View style={{ flex: 1 / 2, flexDirection: "column" }}>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => this.goToScreen(item.product_id)}
                  >
                    <Card padder style={{ borderRadius: 10 }}>
                      <CardItem
                        cardBody
                        style={{ alignSelf: "center", borderRadius: 10 }}
                      >
                        <Image
                          source={{
                            uri:
                              sellerUrl + "images/product/" + item.product_photo
                          }}
                          style={{
                            width: "100%",
                            height: undefined,
                            aspectRatio: 1,
                            borderTopLeftRadius: 8,
                            borderTopRightRadius: 8
                          }}
                        />
                      </CardItem>

                      <CardItem>
                        <Left>
                          <Text>
                            {item.product_name.slice(0, 20) +
                              (item.product_name.length > 20 ? "..." : "")}
                          </Text>
                        </Left>
                      </CardItem>

                      <CardItem footer>
                        <Left>
                          <Text style={style.itemPrice}>
                            US ${item.promotion_price}
                          </Text>
                        </Left>
                      </CardItem>
                    </Card>
                  </TouchableOpacity>
                </View>
              )}
              keyExtractor={(item, index) => item.product_id}
              numColumns={2}
              ListFooterComponent={this.renderLoading.bind(this)}
            />
          </View>
        </Content>
      </Container>
    );
  }
}

export default ProductCategory;

const style = StyleSheet.create({
  container: {
    backgroundColor: "#f7f7f7"
  },
  top: {
    width: screenWidth,
    backgroundColor: "#ff4747"
  },
  header: {
    marginTop: 40,
    backgroundColor: "white"
  },

  headerTitle: {
    fontWeight: "500",
    color: "white"
  },

  itemImage: {
    borderColor: "#ddd",
    borderWidth: 1,
    height: 128,
    width: 128,
    aspectRatio: 1,
    resizeMode: "stretch",
    borderRadius: 8,
    padding: 10,
    marginRight: 5
  },
  itemPrice: {
    fontWeight: "500",
    fontSize: 18,
    color: "black",
    fontStyle: "italic"
  },

  afterDiscount: {
    fontSize: 15,
    color: "#808080",
    textDecorationLine: "line-through",
    marginRight: 5
  }
});
