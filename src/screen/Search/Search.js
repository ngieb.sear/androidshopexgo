import React, { Component } from "react";
import { siteUrl, sellerUrl } from "../../class/Constant";
import ServerClass from "../../class/ServerClass";
import { Navigation } from "react-native-navigation";
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  View
} from "react-native";
import {
  Container,
  Text,
  Content,
  Header,
  Left,
  Button,
  Icon,
  Item,
  Input,
  Card,
  CardItem
} from "native-base";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchData: [],
      offset: 0,
      user: 0
    };
  }

  componentDidMount() {
    ServerClass.checkUser().then(val => {
      this.setState({ user: val });
    });
  }

  backScreen = () => {
    Navigation.dismissModal(this.props.componentId);
  };

  responseSearch = (value, action) => {
    const params = {
      offset: this.state.offset,
      text: value
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseSearch",
      params
    ).then(json => {
      if (action == "scroll") {
        this.setState({
          searchData: this.state.searchData.concat(json),
          text: value
        });
      } else {
        this.setState({
          searchData: json,
          text: value
        });
      }
    });
  };

  goToScreen = (id, screen) => {
    Navigation.push(this.props.componentId, {
      component: {
        name: screen,
        passProps: {
          data: id,
          user: this.state.user
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };

  handleLoadMore = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };

  render() {
    return (
      <Container style={{ backgroundColor: "#f7f7f7" }}>
        <Header
          searchBar
          rounded
          noShadow
          style={{ backgroundColor: "#ff4747" }}
        >
          <Item>
            <Button transparent onPress={() => this.backScreen()}>
              <Icon name="arrow-back" />
            </Button>
            <Input
              placeholder="I'm looking for..."
              onChangeText={val => this.responseSearch(val, "search")}
            />
          </Item>
          <Button transparent>
            <Text>Search</Text>
          </Button>
        </Header>

        <Content
          padder
          onScroll={({ nativeEvent }) => {
            if (this.handleLoadMore(nativeEvent)) {
              this.setState(
                { offset: this.state.offset + 20 },
                this.responseSearch(this.state.text, "scroll")
              );
            }
          }}
        >
          {this.state.searchData.length > 0 ? (
            <FlatList
              data={this.state.searchData}
              renderItem={({ item }) => (
                <View
                  style={{
                    flex: 1 / 2,
                    flexDirection: "column",
                    height: "100%"
                  }}
                >
                  <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => this.goToScreen(item.product_id, "Product")}
                  >
                    <Card padder style={{ borderRadius: 10 }}>
                      <CardItem
                        cardBody
                        style={{ alignSelf: "center", borderRadius: 10 }}
                      >
                        <Image
                          source={{
                            uri:
                              sellerUrl + "images/product/" + item.product_photo
                          }}
                          style={style.itemImage}
                        />
                      </CardItem>

                      <CardItem>
                        <Left>
                          <Text>
                            {item.product_name.slice(0, 20) +
                              (item.product_name.length > 20 ? "..." : "")}
                          </Text>
                        </Left>
                      </CardItem>

                      <CardItem
                        footer
                        style={{
                          borderBottomLeftRadius: 10,
                          borderBottomRightRadius: 10
                        }}
                      >
                        <Left>
                          <Text style={style.itemPrice}>
                            US ${item.promotion_price}
                          </Text>
                        </Left>
                      </CardItem>
                    </Card>
                  </TouchableOpacity>
                </View>
              )}
              keyExtractor={(item, index) => item.product_id}
              numColumns={2}
            />
          ) : (
            <View
              style={{
                backgroundColor: "white"
              }}
            >
              <Image
                source={require("../../assets/icon/empty_search.jpg")}
                style={{
                  width: "100%",
                  height: "100%",
                  aspectRatio: 1,
                  resizeMode: "cover",
                  alignSelf: "center"
                }}
              />
            </View>
          )}
        </Content>
      </Container>
    );
  }
}

export default Search;

const style = StyleSheet.create({
  itemImage: {
    flex: 1,
    width: "100%",
    height: undefined,
    aspectRatio: 1,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8
  },
  itemPrice: {
    fontWeight: "500",
    fontSize: 18,
    color: "black",
    fontStyle: "italic"
  }
});
