import React, { Component } from "react";
import { siteUrl, baseUrl } from "../../class/Constant";
import ServerClass from "../../class/ServerClass";
import {
  Container,
  Header,
  Content,
  Left,
  Icon,
  Card,
  CardItem,
  Body
} from "native-base";

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  ActivityIndicator
} from "react-native";
import { Navigation } from "react-native-navigation";

class Search extends Component {
  mounted = false;
  static options() {
    return {
      topBar: {
        drawBehind: true,
        visible: false,
        animate: false
      }
    };
  }
  constructor(props) {
    super(props);
    this.mounted = false;
    this.state = {
      searchData: [],
      lastID: 0,
      isRefresh: false,
      user: 0
    };
  }
  responseSearch = () => {
    this.setState({ isRefresh: true });
    const params = {
      offset: this.state.lastID,
      text: this.props.type
    };
    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseSearch",
      params
    ).then(json => {
      this.setState({
        searchData: this.state.searchData.concat(json),
        isRefresh: false
      });
    });
  };

  componentDidMount() {
    this.mounted = true;
    ServerClass.checkUser().then(val => {
      this.setState({ user: val });
      this.responseSearch();
    });
  }
  componentWillUnmount() {
    this.mounted = false;
  }
  renderLoading = () => {
    if (!this.state.isLoading) return null;
    return <ActivityIndicator size="large" color="red" />;
  };
  goToScreen = id => {
    if (id != 0) {
      Navigation.push(this.props.componentId, {
        component: {
          name: "Product",
          passProps: {
            data: id,
            user: this.state.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    } else {
      Navigation.pop(this.props.componentId);
    }
  };
  handleLoadMore = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };
  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header
          searchBar
          rounded
          noShadow
          style={{ backgroundColor: "#ff4747" }}
        >
          <Left>
            <Icon
              name="arrow-back"
              style={{ paddingLeft: 10, color: "white" }}
              onPress={() => this.goToScreen(0)}
            />
          </Left>
          <Body>
            <Text style={{ fontSize: 18, color: "white" }}>Search Result</Text>
          </Body>
        </Header>

        <Content
          onScroll={({ nativeEvent }) => {
            if (this.handleLoadMore(nativeEvent)) {
              this.setState(
                { lastID: this.state.lastID + 20 },
                this.responseSearch
              );
            }
          }}
          style={{
            paddingLeft: 13,
            paddingRight: 13,
            backgroundColor: "#f2f2f2"
          }}
        >
          <FlatList
            data={this.state.searchData}
            renderItem={({ item }) => (
              <View
                style={{
                  flex: 1 / 2,
                  flexDirection: "column",
                  height: "100%"
                }}
              >
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => this.goToScreen(item.product_id, "Product")}
                >
                  <Card padder style={{ borderRadius: 10 }}>
                    <CardItem
                      cardBody
                      style={{ alignSelf: "center", borderRadius: 10 }}
                    >
                      <Image
                        source={{
                          uri: baseUrl + "images/product/" + item.product_photo
                        }}
                        style={style.itemImage}
                      />
                    </CardItem>

                    <CardItem>
                      <Left>
                        <Text>
                          {item.product_name.slice(0, 20) +
                            (item.product_name.length > 20 ? "..." : "")}
                        </Text>
                      </Left>
                    </CardItem>

                    <CardItem
                      footer
                      style={{
                        borderBottomLeftRadius: 10,
                        borderBottomRightRadius: 10
                      }}
                    >
                      <Left>
                        <Text style={style.itemPrice}>
                          US ${item.promotion_price}
                        </Text>
                      </Left>
                    </CardItem>
                  </Card>
                </TouchableOpacity>
              </View>
            )}
            keyExtractor={(item, index) => item.product_id}
            numColumns={2}
            ListFooterComponent={this.renderLoading.bind(this)}
          />
        </Content>
      </Container>
    );
  }
}

export default Search;

const style = StyleSheet.create({
  itemImage: {
    flex: 1,
    width: "100%",
    height: undefined,
    aspectRatio: 1,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8
  },
  itemPrice: {
    fontWeight: "500",
    fontSize: 18,
    color: "black",
    fontStyle: "italic"
  }
});
