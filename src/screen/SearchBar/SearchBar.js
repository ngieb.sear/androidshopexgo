import { Container, Header, Item, Button, Icon, Input } from "native-base";
import { Text } from "react-native";
import React, { Component } from "react";
class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Container>
        <Header
          searchBar
          rounded
          noShadow
          style={{ backgroundColor: "#ff4747" }}
        >
          <Item>
            <Input placeholder="Search" />
          </Item>
          <Button transparent>
            <Text>Search</Text>
          </Button>
        </Header>
      </Container>
    );
  }
}

export default SearchBar;
