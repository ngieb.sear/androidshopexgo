import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet } from "react-native";
import { Navigation } from "react-native-navigation";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Icon,
  Left,
  Right,
  Body
} from "native-base";
import ServerClass from "../../class/ServerClass";
class Setting extends Component {
  static options() {
    return {
      topBar: {
        drawBehind: true,
        visible: false,
        animate: false
      }
    };
  }

  goToScreen = screen => {
    if (screen != "back") {
      Navigation.push(this.props.componentId, {
        component: {
          name: screen,
          passProps: {
            user: this.props.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    } else {
      Navigation.pop(this.props.componentId);
    }
  };

  signOut = () => {
    ServerClass.removeUser();
    Navigation.setRoot({
      root: {
        bottomTabs: {
          options: {
            bottomTabs: {
              animate: false,
              titleDisplayMode: "alwaysHide"
            }
          },
          children: [
            {
              stack: {
                children: [
                  {
                    component: {
                      name: "HomeScreen"
                    }
                  }
                ],
                options: {
                  bottomTab: {
                    text: "Home",
                    icon: require("../../assets/icon/home.png")
                  }
                }
              }
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: "Deal"
                    }
                  }
                ],
                options: {
                  bottomTab: {
                    text: "Deal",
                    icon: require("../../assets/icon/power.png")
                  }
                }
              }
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: "Cart"
                    }
                  }
                ],
                options: {
                  bottomTab: {
                    text: "Cart",
                    icon: require("../../assets/icon/cart.png")
                  }
                }
              }
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: "Account"
                    }
                  }
                ],
                options: {
                  bottomTab: {
                    text: "Account",
                    icon: require("../../assets/icon/account.png")
                  }
                }
              }
            }
          ]
        }
      }
    });
  };

  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header searchBar rounded noShadow style={{ backgroundColor: "#000" }}>
          <Left>
            <Icon
              name="arrow-back"
              style={{ paddingLeft: 10, color: "white" }}
              onPress={() => this.goToScreen("back")}
            />
          </Left>
          <Body>
            <Text style={{ fontSize: 18, color: "white" }}>Setting</Text>
          </Body>
          <Right></Right>
        </Header>
        <Content>
          <Card>
            <CardItem button onPress={() => this.goToScreen("Profile")}>
              <Text style={style.content}>Profile</Text>
            </CardItem>
            <CardItem button onPress={() => this.goToScreen("ShippingAddress")}>
              <Text style={style.content}>Shipping Address</Text>
            </CardItem>
            <CardItem>
              <Left>
                <Text style={style.content}>Ship To</Text>
              </Left>
              <Right>
                <Text style={{ fontSize: 12 }}>Available in Cambodia Only</Text>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Text style={style.content}>Rate Loyloy7</Text>
            </CardItem>
            <CardItem
              button
              onPress={() => {
                this.goToScreen("PrivacyPolicy");
              }}
            >
              <Text style={style.content}>Privacy Policy</Text>
            </CardItem>
            <CardItem>
              <Left>
                <Text style={style.content}>Version</Text>
              </Left>
              <Right>
                <Text style={style.content}>1.0.0</Text>
              </Right>
            </CardItem>
          </Card>
          <TouchableOpacity
            style={{
              backgroundColor: "#ff4444",
              alignSelf: "center",
              width: "95%",
              padding: 10,
              marginTop: 10
            }}
            onPress={() => this.signOut()}
          >
            <Text style={{ textAlign: "center", color: "#fff" }}>SIGN OUT</Text>
          </TouchableOpacity>
          <View style={{ alignSelf: "center" }}>
            <Image
              source={require("../../assets/icon/logo.png")}
              style={{ width: null, height: 100 }}
            />
            <Text style={style.buttomContent}>Version 1.0.0</Text>
            <Text style={style.buttomContent}>© 2019-Now LoyLoy7.com.</Text>
            <Text style={style.buttomContent}>All rights reserved.</Text>
          </View>
        </Content>
      </Container>
    );
  }
}

export default Setting;

const style = StyleSheet.create({
  content: {
    color: "#000"
  },
  buttomContent: {
    textAlign: "center",
    color: "#000"
  }
});
