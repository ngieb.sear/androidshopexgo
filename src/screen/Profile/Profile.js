import React, { Component } from "react";

import { Navigation } from "react-native-navigation";

import {
  Platform,
  Dimensions,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  View,
  Image
} from "react-native";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Icon,
  Left,
  Right,
  Body,
  Button
} from "native-base";

import Modal from "react-native-modalbox";

import ServerClass from "../../class/ServerClass";

import { siteUrl } from "../../class/Constant";

const { width: screenWidth } = Dimensions.get("window");

class Profile extends Component {
  mounted = false;
  constructor(props) {
    super(props);
    this.mounted = false;
    this.state = {
      isFalse: false,
      isLoading: false,
      chosenDate: new Date(),
      username: "",
      password: "",
      confirm: "",
      first_name: "",
      last_name: "",
      dob: "",
      email: "",
      user_id: 0
    };
    this.showUpdateModal = this.showUpdateModal.bind(this);
  }

  componentDidMount() {
    this.mounted = true;
    this.responseUsername();
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  responseUsername = () => {
    const params = {
      user: this.props.user
    };

    if (this.mounted) {
      ServerClass.responsePostData(
        siteUrl + "mobileApi/responseUsername",
        params
      ).then(val => {
        this.setState({
          username: val.username,
          first_name: val.first_name,
          last_name: val.last_name,
          dob: val.dob,
          email: val.email
        });
      });
    }
  };

  updateContact = () => {
    var isFalse = false;
    if (this.state.first_name == "") {
      ToastAndroid.show("Please enter your first name !", ToastAndroid.SHORT);
    } else if (this.state.last_name == "") {
      ToastAndroid.show("Please enter your last name !", ToastAndroid.SHORT);
    } else {
      isFalse = true;
    }

    if (isFalse) {
      const params = {
        user: this.props.user,
        first_name: this.state.first_name,
        last_name: this.state.last_name
      };

      ServerClass.responsePostData(
        siteUrl + "mobileApi/updateContactName",
        params
      ).then(json => {
        if (json.statusCode == "S0001") {
          this.responseUsername();
          this.refs.contactName.close();
        }
      });
    }
  };

  showUpdateModal() {
    this.refs.contactName.open();
  }

  goToScreen = screen => {
    if (screen == "back") {
      Navigation.pop(this.props.componentId);
    } else {
      Navigation.push(this.props.componentId, {
        component: {
          name: screen,
          passProps: {
            user: this.props.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    }
  };

  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header noShadow style={{ backgroundColor: "#000" }}>
          <Left>
            <Icon
              name="arrow-back"
              style={{ paddingLeft: 10, color: "white" }}
              onPress={() => this.goToScreen("back")}
            />
          </Left>
          <Body>
            <Text style={{ fontSize: 18, color: "white" }}>Profile</Text>
          </Body>
          <Right>
            <Button transparent onPress={() => this.responseUsername()}>
              <Icon name="refresh" />
            </Button>
          </Right>
        </Header>
        <Content>
          <Card>
            <CardItem>
              <Left>
                <Text style={style.textStyle}>My Photo</Text>
              </Left>
              <Right>
                <Image
                  source={require("../../assets/icon/user-account.png")}
                  style={{ width: 40, height: 40, borderRadius: 50 }}
                />
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem button onPress={() => this.showUpdateModal()}>
              <Left>
                <Text style={style.textStyle}>Contact Name</Text>
              </Left>
              <Right>
                <Text style={style.textStyle}>{this.state.username}</Text>
              </Right>
            </CardItem>
            <CardItem>
              <Left>
                <Text style={style.textStyle}>Account Info</Text>
              </Left>
              <Right>
                <Text style={{ fontSize: 12 }}>{this.state.email}</Text>
              </Right>
            </CardItem>

            <CardItem button onPress={() => this.goToScreen("UpdateDob")}>
              <Left>
                <Text style={style.textStyle}>Birth Year</Text>
              </Left>
              <Right>
                <Text style={style.textStyle}>{this.state.dob}</Text>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem button onPress={() => this.goToScreen("ShippingAddress")}>
              <Text style={style.textStyle}>Shipping Address</Text>
            </CardItem>
          </Card>
        </Content>
        <Modal
          ref={"contactName"}
          style={{
            justifyContent: "center",
            borderRadius: Platform.OS === "ios" ? 10 : 10,
            shadowRadius: 10,
            width: screenWidth - 80,
            height: 300
          }}
          backdrop={true}
        >
          <Text
            style={{
              fontSize: 15,
              fontWeight: "bold",
              textAlign: "center"
            }}
          >
            Contact Name
          </Text>

          <TextInput
            value={this.state.first_name}
            style={style.textInput}
            onChangeText={val => this.setState({ first_name: val })}
          ></TextInput>

          <TextInput
            value={this.state.last_name}
            style={style.textInput}
            onChangeText={val => this.setState({ last_name: val })}
          ></TextInput>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-end",
              alignItems: "flex-end"
            }}
          >
            <TouchableOpacity
              style={{ marginLeft: 30, marginRight: 30 }}
              onPress={() => this.updateContact()}
            >
              <Text style={{ fontSize: 18, color: "#ff4747" }}>Update</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{ marginRight: 30 }}
              onPress={() => this.refs.myModal.close()}
            >
              <Text style={{ fontSize: 18, color: "#ff4747" }}>Close</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </Container>
    );
  }
}

export default Profile;

const style = StyleSheet.create({
  textStyle: {
    color: "#000"
  },
  textInput: {
    borderBottomColor: "grey",
    marginLeft: 30,
    marginRight: 30,
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: 1
  }
});
