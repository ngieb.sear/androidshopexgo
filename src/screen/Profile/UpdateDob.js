import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Icon,
  Text,
  Content,
  Title,
  Item,
  Label,
  Footer,
  FooterTab,
  DatePicker
} from "native-base";

import { StyleSheet, View, ActivityIndicator } from "react-native";

import { siteUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

import { Navigation } from "react-native-navigation";

import moment from "moment";

class UpdateDob extends Component {
  mounted = false;
  constructor(props) {
    super(props);
    this.mounted = false;
    this.state = {
      isFalse: false,
      isLoading: false,
      chosenDate: new Date(),
      dob: ""
    };
  }

  componentWillMount() {
    this.mounted = false;
  }

  componentDidMount() {
    this.mounted = true;
    this.responseDob();
  }

  responseDob = () => {
    const params = {
      user: this.props.user
    };

    if (this.mounted) {
      ServerClass.responsePostData(
        siteUrl + "mobileApi/responseUsername",
        params
      ).then(val => {
        this.setState({
          chosenDate: val.user_dob
        });
      });
    }
  };

  saveDob = () => {
    const params = {
      user: this.props.user,
      dob: this.state.chosenDate
    };

    ServerClass.responsePostData(siteUrl + "mobileApi/saveDob", params).then(
      json => {
        if (json.statusCode == "S0001") {
          Navigation.pop(this.props.componentId);
        }
      }
    );
  };

  backToScreen = () => {
    Navigation.pop(this.props.componentId);
  };

  render() {
    return (
      <Container style={{ backgroundColor: "white" }}>
        <Header noShadow style={{ backgroundColor: "black" }}>
          <Left>
            <Button transparent onPress={() => this.backToScreen()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Birthday</Title>
          </Body>
        </Header>
        <Content padder>
          <Item stackedLabel>
            <Label>Birthday</Label>

            <DatePicker
              defaultDate={new Date()}
              maximumDate={new Date()}
              locale={"kh"}
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType={"fade"}
              androidMode={"default"}
              placeHolderText="Select Birth of Date"
              textStyle={{
                color: "black",
                fontSize: 16,
                fontWeight: "500"
              }}
              placeHolderTextStyle={{
                color: "black",
                fontSize: 16,
                fontWeight: "500"
              }}
              onDateChange={date => {
                this.setState({
                  chosenDate: moment(date).format("YYYY-MM-DD")
                });
              }}
              disabled={false}
            />
          </Item>
        </Content>
        <Footer>
          <FooterTab>
            <Button style={style.buy} onPress={() => this.saveDob()}>
              <Text style={style.buyText}>Save</Text>
            </Button>
          </FooterTab>
        </Footer>
        {this.state.isLoading ? (
          <View style={style.spinnerBackground}>
            <View style={style.spinnerView}>
              <ActivityIndicator size="large" color="red" />
            </View>
          </View>
        ) : null}
      </Container>
    );
  }
}

export default UpdateDob;

const style = StyleSheet.create({
  buy: {
    backgroundColor: "#ff4747",
    width: "100%",
    borderRadius: 5,
    alignContent: "center",
    justifyContent: "center"
  },
  buyText: {
    fontSize: 15,
    fontWeight: "500",
    color: "white",
    alignSelf: "center"
  },
  spinnerView: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  spinnerBackground: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.5)"
  }
});
