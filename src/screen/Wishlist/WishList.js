import React, { Component } from "react";

import {
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
  Title,
  Text,
  Card,
  CardItem,
  Button,
  Icon
} from "native-base";

import {
  StyleSheet,
  Image,
  CheckBox,
  TouchableOpacity,
  View,
  FlatList,
  ActivityIndicator,
  Dimensions,
  ToastAndroid,
  Alert
} from "react-native";

import { Navigation } from "react-native-navigation";

import { siteUrl, baseUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

const { width: screenWidth } = Dimensions.get("window");

class WishList extends Component {
  mounted = false;
  constructor(props) {
    super(props);

    this.state = {
      wishList: [],
      user: 0,
      lastID: 0,
      qty: 0,
      moreToLove: [],
      checkAll: false,
      checkItem: [],
      totalAmount: 0.0,
      countItem: 0
    };
  }

  componentDidMount = () => {
    this.mounted = true;
    this.responseWishList();
  };

  componentWillUnmount() {
    this.mounted = false;
  }

  responseWishList = () => {
    if (this.props.user > 0) {
      const params = {
        user_id: this.props.user
      };
      if (this.mounted) {
        ServerClass.responsePostData(
          siteUrl + "mobileApi/responseWishList",
          params
        ).then(json => {
          this.setState({ wishList: json });
        });
      }
    }
    this.responseProduct();
  };

  responseProduct = () => {
    const params = {
      offset: this.state.lastID
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseProduct",
      params
    ).then(json => {
      this.setState({
        moreToLove: this.state.moreToLove.concat(json)
      });
    });
  };

  handleLoadMore = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };

  goToScreen = (id, screen) => {
    Navigation.push(this.props.componentId, {
      component: {
        name: screen,
        passProps: {
          data: id,
          user: this.props.user
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };

  handleCheckItem(id, index) {
    const newArray = [...this.state.wishList];
    newArray[index].checked = !newArray[index].checked;

    this.setState({
      wishList: newArray,
      checkAll: false
    });
  }

  removeItemFromWishList = () => {
    let wishLength = this.state.wishList.length;
    const wishData = [...this.state.wishList];
    var deleteArr = [];
    for (i = 0; i < wishLength; i++) {
      if (wishData[i].checked) {
        deleteArr.push(wishData[i].wishlist_id);
      }
    }
    if (deleteArr.length > 0) {
      Alert.alert(
        "Remove",
        "Are you sure you want to remove the items selected ?",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          {
            text: "Remove",
            onPress: () => {
              const params = {
                detail_id: deleteArr
              };

              ServerClass.responsePostData(
                siteUrl + "mobileApi/removeItemFromWishList",
                params
              ).then(json => {
                if (json.statusCode == "S0001") {
                  this.responseWishList();
                  ToastAndroid.show(
                    " Wish list item(s) has been removed ",
                    ToastAndroid.SHORT
                  );
                }
              });
            }
          }
        ],
        { cancelable: false }
      );
    } else {
      ToastAndroid.show(
        "Please select which item you want to remove ",
        ToastAndroid.SHORT
      );
    }
  };

  render() {
    return (
      <Container style={style.container}>
        <Header noShadow style={{ backgroundColor: "black" }}>
          <Left>
            <Button
              transparent
              onPress={() => Navigation.pop(this.props.componentId)}
            >
              <Icon name="arrow-back" />
            </Button>
          </Left>

          <Body>
            <Title style={style.headerTitle}>Wish List</Title>
          </Body>

          <Right>
            <Button
              vertical
              transparent
              onPress={() => this.goToScreen(0, "ViewCart")}
            >
              <Icon name="cart" />
            </Button>
            <Button
              vertical
              transparent
              onPress={() => this.removeItemFromWishList()}
            >
              <Icon name="trash" />
            </Button>
          </Right>
        </Header>

        <Content
          onScroll={({ nativeEvent }) => {
            if (this.handleLoadMore(nativeEvent)) {
              this.setState(
                { lastID: this.state.lastID + 20 },
                this.responseProduct
              );
            }
          }}
        >
          {this.state.wishList.length > 0 ? (
            this.state.wishList.map((item, index) => {
              return (
                <Card transparent key={item.wishlist_id}>
                  <CardItem
                    button
                    onPress={() => this.goToScreen(item.product_id, "Product")}
                  >
                    <Left>
                      <CheckBox
                        value={item.checked}
                        onChange={() =>
                          this.handleCheckItem(item.product_id, index)
                        }
                      ></CheckBox>
                      <Image
                        source={{
                          uri: baseUrl + "images/product/" + item.product_photo
                        }}
                        style={style.itemImage}
                      />
                    </Left>
                    <Body>
                      <Text>
                        {item.product_name_en.slice(0, 50) +
                          (item.product_name_en.length > 50 ? "..." : "")}
                      </Text>

                      <Text style={style.itemPrice}>
                        US ${item.product_price}
                      </Text>
                    </Body>
                  </CardItem>
                </Card>
              );
            })
          ) : (
            <Card transparent>
              <CardItem cardBody>
                <Image
                  source={require("../../assets/icon/empty_cart.jpg")}
                  style={{
                    width: "100%",
                    height: "100%",
                    aspectRatio: 1,
                    resizeMode: "cover",
                    alignSelf: "center"
                  }}
                />
              </CardItem>
            </Card>
          )}

          <View style={{ paddingLeft: 13, paddingRight: 13, marginTop: 10 }}>
            <Text style={{ fontWeight: "500", fontSize: 18, color: "black" }}>
              More To Love
            </Text>
            <FlatList
              data={this.state.moreToLove}
              renderItem={({ item }) => (
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => this.goToScreen(item.product_id, "Product")}
                  >
                    <Card padder style={{ borderRadius: 10 }}>
                      <CardItem
                        cardBody
                        style={{ alignSelf: "center", borderRadius: 10 }}
                      >
                        <Image
                          source={{
                            uri:
                              baseUrl + "images/product/" + item.product_photo
                          }}
                          style={{
                            width: "100%",
                            height: undefined,
                            aspectRatio: 1,
                            borderTopLeftRadius: 8,
                            borderTopRightRadius: 8
                          }}
                        />
                      </CardItem>

                      <CardItem>
                        <Left>
                          <Text>
                            {item.product_name.slice(0, 20) +
                              (item.product_name.length > 20 ? "..." : "")}
                          </Text>
                        </Left>
                      </CardItem>

                      <CardItem footer>
                        <Left>
                          <Text style={style.itemPrice}>
                            US ${item.promotion_price}
                          </Text>
                        </Left>
                      </CardItem>
                    </Card>
                  </TouchableOpacity>
                </View>
              )}
              keyExtractor={(item, index) => item.product_id}
              numColumns={2}
            />
          </View>
          <ActivityIndicator size="large" color="red" />
        </Content>
      </Container>
    );
  }
}

export default WishList;

const style = StyleSheet.create({
  container: {
    backgroundColor: "#f7f7f7"
  },
  top: {
    width: screenWidth,
    backgroundColor: "black"
  },
  header: {
    marginTop: 40,
    backgroundColor: "white"
  },

  headerTitle: {
    fontWeight: "500",
    color: "white"
  },

  itemImage: {
    borderColor: "#ddd",
    borderWidth: 1,
    height: 128,
    width: 128,
    aspectRatio: 1,
    resizeMode: "stretch",
    borderRadius: 8,
    padding: 10,
    marginRight: 5
  },
  itemPrice: {
    fontWeight: "500",
    fontSize: 18,
    color: "black",
    fontStyle: "italic"
  },
  totalAmount: {
    fontWeight: "500",
    fontSize: 18,
    color: "black"
  },
  buy: {
    backgroundColor: "#ff4747",
    width: "100%",
    borderRadius: 5
  },
  buyText: {
    fontSize: 15,
    fontWeight: "500",
    color: "white"
  }
});
