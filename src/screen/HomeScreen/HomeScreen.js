import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Left,
  Input,
  Item,
  Icon,
  Text,
  Badge,
  Card,
  CardItem,
  Button,
  Body
} from "native-base";

import {
  StyleSheet,
  Image,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  ScrollView,
  View,
  ActivityIndicator,
  ProgressBarAndroid,
  AppRegistry,
  YellowBox
} from "react-native";

import { siteUrl, sellerUrl, adminUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

import { Grid, Row, Col } from "react-native-easy-grid";

import Carousel from "react-native-snap-carousel";

import { Navigation } from "react-native-navigation";

const { width: screenWidth } = Dimensions.get("window");

import { connect } from "react-redux";

import CountDown from 'react-native-countdown-component';
//import HideableView from 'react-native-hideable-view/HideableView';

class HomeScreen extends Component {
  mounted = false;
  static options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
        animate: false
      }
    };
  }

  constructor(props) {
    super(props);
    this.mounted = false;
    this.state = {
      slideItem: [],
      FlashDeal: [],
      moreToLove: [],
      lastID: 0,
      isRefresh: false,
      user: 0,
      FlashDealDay: 300,
      FlashDealHour: 0,
      FlashDealMinute: 0,
      FlashDealSecond: 0,
      FlashDealVisible: true
    };
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillUpdate is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
  }

  componentDidMount() {
    this.mounted = true;
    ServerClass.checkUser().then(val => {
      this.setState({ user: val });
      this.responseSlide();
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  GetFlashDealTime = () =>{
    let day = this.state.FlashDealDay;
    let hour = this.state.FlashDealHour;
    let minute = this.state.FlashDealMinute;
    let second = this.state.FlashDealSecond -1;
    if(day==0 && hour==0 && minute==0){
      day=0;
      hour=0;
      minute=0;
      second=0;
    } else {
        if(second<=0) {second = 59; minute = minute -1;}
        if(minute<=0) {
           if(hour<=0){
             minute=0;
           }else {
             minute=59;
             hour = hour -1;
           }
        }
        if(hour<=0) {hour = 0; day = day -1;}
        if(day<=0) {day = 0;}
    }
    
    this.setState({FlashDealDay: day, FlashDealHour : hour, FlashDealMinute: minute, FlashDealSecond: second});
  }

  componentDidUpdate(prevProps) {
    if (this.props.userID.user_id !== prevProps.userID.user_id) {
      ServerClass.checkUser().then(val => {
        this.setState({ user: val });
      });
    }
  }

  responseSlide = () => {
    ServerClass.responsGetData(siteUrl + "mobileApi/responseSlide").then(
      json => {
        if (this.mounted) {
          this.setState({ slideItem: json });
          this.responseFlashDeal();
          this.responseProduct();
        }
      }
    );
  };

  responseFlashDeal = () => {
      ServerClass.responsGetData(siteUrl + "mobileApi/responseHotdeal").then(
      json => {
        this.setState({ FlashDeal: json });
      }
    );
    // if(this.state.FlashDeal != '' || this.state.FlashDeal != null){
    //   this.setState({FlashDealDay : 1, FlashDealHour : 1, FlashDealMinute: 2, FlashDealSecond: 15});
    //   this.GetFlashDealTime();
    //   this.timer = setInterval(() => {
    //     this.GetFlashDealTime();
    //   }, 1000);
    // }
  };

  responseProduct = () => {
    this.setState({ isRefresh: true });
    const params = {
      offset: this.state.lastID
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseProduct",
      params
    ).then(json => {
      this.setState({
        moreToLove: this.state.moreToLove.concat(json),
        isRefresh: false
      });
    });
  };

  _renderSlide({ item, index }) {
    return (
      <Card style={{ borderRadius: 10 }}>
        <CardItem cardBody style={{ borderRadius: 10 }}>
          <Image
            source={{ uri: adminUrl + "images/slide/" + item.slide_photo }}
            //source={require('./NoImage.png')} 
            style={{ height: 200, width: null, flex: 1, borderRadius: 10 }}
          />
        </CardItem>
      </Card>
    );
  }

  goToScreen = (id, screen) => {
    if (screen == "Cate") {
      Navigation.push(this.props.componentId, {
        component: {
          name: "Promotion",
          passProps: {
            id: id,
            user: this.state.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    } else {
      Navigation.push(this.props.componentId, {
        component: {
          name: "Product",
          passProps: {
            data: id,
            user: this.state.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    }
  };

  handleLoadMore = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };

  searchModal = () => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "Search",
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                bottomTabs: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                animations: {
                  setStackRoot: {
                    enabled: true
                  }
                }
              }
            }
          }
        ]
      }
    });
  };

  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header
          searchBar
          rounded
          noShadow
          style={{ backgroundColor: "#ff4747" }}
        >
          <Item>
            <Icon name="ios-search" />
            <Input
              placeholder="I'm looking for..."
              onTouchStart={() => this.searchModal()}
              //editable={false}
            />
          </Item>
          <Button transparent>
            <Text>Search</Text>
          </Button>
        </Header>
        <Content
          onScroll={({ nativeEvent }) => {
            if (this.handleLoadMore(nativeEvent)) {
              this.setState(
                { lastID: this.state.lastID + 20 },
                this.responseProduct
              );
            }
          }}
        >
          <ImageBackground
            source={require("../../assets/icon/contentBg.png")}
            style={{ width: "100%" }}
          >
            <View>
              <Carousel
                layout={"default"}
                ref={ref => (this.carousel = ref)}
                data={this.state.slideItem}
                sliderWidth={screenWidth}
                sliderHeight={screenWidth}
                itemWidth={screenWidth - 25}
                renderItem={this._renderSlide}
                loop={true}
                autoplay={true}
              />
            </View>
          </ImageBackground>

          <Grid style={{ marginTop: 10 }}>
            <Row style={{ paddingLeft: 13, paddingRight: 13 }}>
              {this.state.FlashDeal.length>0 ? (
                // <HideableView visible={this.state.FlashDealVisible}>
                  <View style={{width: '100%'}}>
                  <View style={{width: '100%', flexDirection: 'row', marginBottom: 5}}>
                    <View style={{width: 100}}>
                      <Title style={style.title}>Flash Deal</Title>
                    </View>
                    <CountDown
                        until={this.state.FlashDealDay}
                        size={12}
                        //onFinish={() => this.setState({FlashDealVisible: false}) /*alert('Finished')*/}
                        digitStyle={{backgroundColor: 'black'}}
                        digitTxtStyle={{color: 'white'}}
                        separatorStyle={{color: 'black'}}
                        timeToShow={['D', 'H', 'M', 'S']}
                        timeLabels={{m: null, s: null}}
                        showSeparator
                    />
                  </View>
                  <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    {this.state.FlashDeal.map(item => {
                      return (
                        <TouchableOpacity
                          style={style.btnPromotion}
                          key={item.product_id}
                          onPress={() =>
                            this.goToScreen(item.product_id, "Cate")
                          }
                        >
                          <ImageBackground 
                            //source={{uri: siteUrl + "images/product/" + item.product_photo}}
                            source={require('./NoImage.png')} 
                            style={style.FlashDealImg}>
                              <View style={style.viewHoldonImg,{flexDirection: 'row-reverse'}}>
                                <Badge danger>
                                  <Text style={{fontSize: 14}}>{item.promotion_discount}%</Text>
                                </Badge>
                              </View>
                          </ImageBackground>
                          <Text style={style.FlashDealText}>
                            US ${item.product_price}
                          </Text>
                          {/* <ProgressBarAndroid
                                styleAttr="Horizontal"
                                indeterminate={false}
                                progress={0.5}
                                style={style.progressBar}
                              /> */}
                          <Text style={style.FlashDealText}>10 sold</Text>
                        </TouchableOpacity>
                      );
                    })}
                </ScrollView>
              </View>
                // </HideableView>
              ) : null}
            </Row>
            <Row style={{ paddingLeft: 13, paddingRight: 13 }} />

            <FeatureCate comID={this.props.componentId} user={this.state.user} />

            <Row style={{ marginTop: 10, marginBottom: 10 }}>
              <View style={{ width: "100%", alignItems: "center",flexDirection: 'row',paddingLeft: 13 }}>
                <Title style={style.title}>More To Love</Title>
              </View>
            </Row>
          </Grid>

          <View style={{width: '100%',paddingLeft: 13, paddingRight: 13}}>
            <FlatList
              data={this.state.moreToLove}
              contentContainerStyle={{ flexGrow: 1 }}
              renderItem={({ item }) => (
                <View
                  style={{
                    flex: 1 / 2
                  }}
                >
                  <Card style={{ borderRadius: 10 }}>
                    <CardItem
                      cardBody
                      style={{ alignSelf: "center", borderRadius: 10 }}
                      button
                      onPress={() => this.goToScreen(item.product_id, "Product")}
                    >
                      <Image
                        source={{ uri: sellerUrl + "images/product/" + item.product_photo}}
                        //source={require('./NoImage.png')}  
                        style={style.itemImage}
                      />
                    </CardItem>

                    <CardItem
                      style={{ height: 50 }}
                      button
                      onPress={() => this.goToScreen(item.product_id, "Product")}
                    >
                      <Body>
                        <Text style={style.itemText}>
                          {item.product_name.slice(0, 35) +
                            (item.product_name.length > 35 ? "..." : "")}
                        </Text>
                      </Body>
                    </CardItem>

                    <CardItem
                      footer
                      style={{
                        borderBottomLeftRadius: 10,
                        borderBottomRightRadius: 10
                      }}
                      button
                      onPress={() => this.goToScreen(item.product_id, "Product")}
                    >
                      <Left>
                        <Text style={style.itemPrice}>
                          US ${item.promotion_price}
                        </Text>
                      </Left>
                    </CardItem>
                  </Card>
                </View>
              )}
              keyExtractor={(item, index) => item.product_id}
              numColumns={2}
            />
          </View>
          
          <ActivityIndicator
            size="large"
            color="red"
            style={{ alignSelf: "center" }}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    userID: state
  };
};

export default connect(mapStateToProps)(HomeScreen);

class FeatureCate extends Component {
  constructor() {
    super();
    this.state = {
      hotCategory:  []
      //   {"testing":  [
      //     {
      //       "product_id": "1",
      //       "product_name": "Stationery",
      //       "product_photo": "08060720190629Punto.webp"
      //     },
      //     {
      //       "product_id": "4",
      //       "product_name": "Testing 4 index",
      //       "product_photo": "08060220190629ECO.webp"
      //     },
      //     {
      //       "product_id": "8",
      //       "product_name": "Testing 8 index",
      //       "product_photo": "08072020190706Simple.webp"
      //     }
      //   ]
      // }
    };
  }

  componentDidMount() {
    this.mounted = true;
    this.responseHotCategory();
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  responseHotCategory = () => {
    if (this.mounted) {
      ServerClass.responsGetData(siteUrl + "mobileApi/responseCategory").then(
      //ServerClass.responsGetData(siteUrl + "mobileApi/responseHotdeal").then(
        json => {
          this.setState({ hotCategory: json });
        }
      );
    }
  };

  goToCategory = () => {
    Navigation.push(this.props.comID, {
      component: {
        name: "Category",
        passProps: {
          user: this.props.user
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };
  render() {
    return (
      <Row style={{ paddingLeft: 13, paddingRight: 13}}>
        {this.state.hotCategory.length > 0 ? (
          <View style={{ width: "100%"}}>
            <View style={{marginBottom: 5,flexDirection: 'row'}}>
                <Title style={style.title}>Featured Categories</Title>
            </View>
              <FlatList
                data={this.state.hotCategory}
                contentContainerStyle={{ flexGrow: 1 }}
                renderItem={({ item }) => (
                  <View style={{ flex: 1 / 2, paddingRight: 3, paddingBottom: 5}}>
                    <TouchableOpacity
                      key={item.product_category_id}
                      onPress={() => this.goToCategory()}
                    >
                      <Image
                          //source={{ uri:  sellerUrl + "img/icon/" + item.product_category_icon }}
                          source={require('./NoImage.png')}
                          style={{height: undefined, width: "100%", aspectRatio: 1.5,
                                  resizeMode: "stretch",borderTopLeftRadius: 10, borderTopRightRadius: 10
                                }}
                        /> 
                        <Text style={style.bgText}>
                            {item.product_category_name}
                        </Text>
                    </TouchableOpacity>
                  </View>
                )}
                keyExtractor={(item, index) => item.product_category_id}
                numColumns={4}
              />
          </View>
        ) : null}
      </Row>
    );
  }
}

const style = StyleSheet.create({
  container: {
    backgroundColor: "#f7f7f7"
  },
  header: {
    backgroundColor: "#e40046"
  },
  headerSearch: {
    borderRadius: 25
  },

  btnPromotion: {
    width: 100,
    height: null
  },
  itemImage: {
    width: "100%",
    height: "100%",
    aspectRatio: 1,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8
  },
  itemPrice: {
    fontWeight: "bold",
    fontSize: 12,
    color: "black"
  },
  itemText: {
    fontWeight: "bold",
    fontSize: 10,
    color: "black"
  },
  FlashDealImg: {
    width: 90,
    height: 90,
    alignSelf: "center",
    borderRadius: 10
  },
  FlashDealText: {
    textAlign: "center",
    fontSize: 12,
    color: "black",
    fontWeight: "bold",
  },
  progressBar: {
    height: 7,
    width: 80,
    //backgroundColor: 'white',
    //borderColor: '#ff5c33',
    borderWidth: 2,
    borderRadius: 5,
    color: "#ff5c33",
    alignSelf: "center"
  },
  viewHoldonImg: {
    position: 'absolute', 
    top: 0, left: 0, right: 0, bottom: 0
  },
  bgText: {
    height: 30,
    width: '100%',
    fontSize: 12,
    color: "black",
    fontWeight: "bold",
    textAlign: "center",
    //alignSelf: "center",
    //justifyContent: 'center',
    textAlignVertical: "center",
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    color: "white",
    marginTop: -1,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  textClock: {
    color: 'white', fontWeight: 'bold',textAlign: 'center', fontSize: 16
  },
  textPoint: {
    color: 'black',fontWeight: 'bold',textAlign: 'center',fontSize: 16
  },
  viewClock: {
    width: 25, height: 25,backgroundColor: 'black'
  },
  viewPoint: {
    width: 5, height: 25, marginLeft: 3, marginRight: 3
  },
  title: {
    fontWeight: 'bold', color: 'black'
  }
});
