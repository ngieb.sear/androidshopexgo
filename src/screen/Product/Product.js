import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Text,
  Thumbnail,
  Content,
  Card,
  CardItem,
  Subtitle,
  Badge,
  Footer,
  FooterTab
} from "native-base";

import {
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  Dimensions,
  FlatList,
  ToastAndroid,
  ActivityIndicator,
  Share
} from "react-native";

import { Navigation } from "react-native-navigation";

import Carousel from "react-native-snap-carousel";

import { siteUrl, sellerUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

import { Grid, Row, Col } from "react-native-easy-grid";

import { connect } from "react-redux";

const { width: screenWidth } = Dimensions.get("window");

class Product extends Component {
  mounted = false;
  constructor(props) {
    super(props);

    this.state = {
      itemImage: [],
      itemInfo: [],
      itemColor: [],
      itemDesc: [],
      itemSimilar: [],
      itemData: [],
      offset: 0,
      user: 0,
      price: 0,
      showWishList: require("../../assets/icon/beforeAdd.png"),
      test: false,
      countOrder: 0,
      hasPushed: false,
      active: false
    };

    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentDidMount = () => {
    this.mounted = true;

    this.responseCountItem();
    this.responseProductDetail();
  };

  componentWillMount = () => {
    this.checkWishList();
  };

  componentWillUnmount() {
    this.mounted = false;

    if (this.navigationEventListener) {
      this.navigationEventListener.remove();
    }
  }

  componentDidAppear() {
    const { hasPushed } = this.state;

    if (hasPushed) {
      ServerClass.checkUser().then(val => {
        this.setState({ user: val });
        this.checkWishList();
        this.setState({ hasPushed: false });
      });
    }
  }

  responseCountItem = () => {
    if (this.props.user != 0) {
      const params = {
        user_id: this.props.user
      };

      ServerClass.responsePostData(
        siteUrl + "mobileApi/responseCountItem",
        params
      ).then(json => {
        this.props.countCartReducer(json.count);
      });
    } else {
      this.props.countCartReducer(0);
    }
  };

  responseProductDetail = () => {
    if (this.mounted) {
      const params = {
        id: this.props.data
      };

      ServerClass.responsePostData(
        siteUrl + "mobileApi/responseProductDetail",
        params
      ).then(json => {
        var i, j;
        var itemImage = [];
        var itemColor = [];
        var itemData = [];
        var price = 0;

        for (i = 0; i < json.Product.length; i++) {
          itemData.push(json.Product[i]);
          itemImage.push({ image: json.Product[i].product_photo });
          price = json.Product[i].promotion_price;
          for (j = 0; j < json.Product[i].Detail.length; j++) {
            itemImage.push({ image: json.Product[i].Detail[j].product_photo });
            itemColor.push({
              detail_id: json.Product[i].Detail[j].product_detail_id,
              color_id: json.Product[i].Detail[j].product_color_id,
              color_name: json.Product[i].Detail[j].color_name,
              image: json.Product[i].Detail[j].product_photo
            });
          }
        }

        this.setState({
          itemImage: itemImage,
          itemInfo: json.Product,
          itemColor: itemColor,
          itemData: itemData,
          price: price
        });

        this.responseProductDescription();
      });
    }
  };

  _renderProduct({ item, index }) {
    return (
      <Image
        source={{
          uri: sellerUrl + "images/product/" + item.image
        }}
        style={style.itemImage}
      />
    );
  }

  responseProductDescription = () => {
    const params = {
      id: this.props.data
    };
    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseProductDescription",
      params
    ).then(json => {
      this.setState({
        itemDesc: json
      });
    });
  };

  responseSimilarProduct = () => {
    const params = {
      id: this.props.data,
      offset: this.state.offset
    };
    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseSimilarProduct",
      params
    ).then(json => {
      this.setState({
        itemSimilar: this.state.itemSimilar.concat(json)
      });
    });
  };

  goToScreen = id => {
    if (id != 0) {
      Navigation.push(this.props.componentId, {
        component: {
          name: "Product",
          passProps: {
            data: id,
            user: this.props.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    } else {
      Navigation.push(this.props.componentId, {
        component: {
          name: "ViewCart",
          passProps: {
            user: this.props.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    }
  };

  handleLoadMore = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };

  backToScreen = () => {
    Navigation.pop(this.props.componentId);
  };

  addWishList = () => {
    if (this.props.user > 0) {
      const params = {
        id: this.props.data,
        user: this.props.user
      };
      ServerClass.responsePostData(
        siteUrl + "mobileApi/addWishList",
        params
      ).then(json => {
        if (json.statusCode == "E0002") {
          ToastAndroid.show(
            "This item is already in your wishlist !",
            ToastAndroid.SHORT
          );
        } else {
          this.setState({
            showWishList: require("../../assets/icon/afterAdd.png")
          });
          ToastAndroid.show("Added !", ToastAndroid.SHORT);
        }
      });
    } else {
      Navigation.push(this.props.componentId, {
        component: {
          name: "JoinOrSignIn",
          passProps: {
            proID: this.props.data,
            status: "wishlist"
          },

          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });

      this.setState({ hasPushed: true });
    }
  };

  checkWishList = () => {
    var user = "";
    if (this.props.user != 0) {
      user = this.props.user;
    } else {
      user = this.state.user;
    }
    const params = {
      id: this.props.data,
      user: user
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/checkWishList",
      params
    ).then(json => {
      if (json.statusCode == "S0001") {
        this.setState({
          showWishList: require("../../assets/icon/afterAdd.png")
        });
      }
    });
  };

  addToCart = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "ProductOption",
        passProps: {
          id: this.props.data,
          itemData: this.state.itemData,
          itemColor: this.state.itemColor,
          price: this.state.price,
          user: this.props.user,
          status: "add"
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };

  buyNow = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "ProductOption",
        passProps: {
          id: this.props.data,
          itemData: this.state.itemData,
          itemColor: this.state.itemColor,
          price: this.state.price,
          user: this.props.user,
          status: "buy"
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          "https://www.loyloy7.com/product/single_product/" + this.props.data
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          ToastAndroid.show("Item has been shared ! ", ToastAndroid.SHORT);
        } else {
        }
      } else if (result.action === Share.dismissedAction) {
        ToastAndroid.show("Canceled ! ", ToastAndroid.SHORT);
      }
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header style={{ backgroundColor: "#ff4747" }}>
          <Left>
            <Button transparent onPress={() => this.backToScreen()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Detail</Title>
          </Body>
          <Right>
            <Button
              badge
              vertical
              transparent
              onPress={() => this.goToScreen(0)}
            >
              <Badge style={{ position: "absolute", left: 35, zIndex: 1 }}>
                <Text>{this.props.countItem.countCartReducer}</Text>
              </Badge>
              <Thumbnail
                square
                small
                source={require("../../assets/icon/white-cart.png")}
              />
            </Button>

            <Button transparent onPress={() => this.onShare()}>
              <Thumbnail
                square
                small
                source={require("../../assets/icon/share.png")}
              />
            </Button>
          </Right>
        </Header>

        <Content
          onScroll={({ nativeEvent }) => {
            if (this.handleLoadMore(nativeEvent)) {
              this.setState(
                { offset: this.state.offset + 20 },
                this.responseSimilarProduct
              );
            }
          }}
        >
          <Carousel
            layout={"default"}
            ref={ref => (this.carousel = ref)}
            data={this.state.itemImage}
            sliderWidth={screenWidth}
            sliderHeight={screenWidth}
            itemWidth={screenWidth}
            renderItem={this._renderProduct}
          />

          {this.state.itemInfo.map((item, i) => {
            return (
              <Card key={i}>
                <CardItem header>
                  <Left>
                    <Grid>
                      {item.promotion_discount != 0 ? (
                        <Row>
                          <Subtitle style={style.afterDiscount}>
                            US ${item.retail_price}
                          </Subtitle>
                          <Badge>
                            <Text style={{ fontSize: 12 }}>
                              -{item.promotion_discount}%
                            </Text>
                          </Badge>
                        </Row>
                      ) : null}

                      <Row>
                        <Title
                          style={{
                            fontSize: 20,
                            color: "black",
                            fontWeight: "bold"
                          }}
                        >
                          US ${item.promotion_price}
                        </Title>
                      </Row>
                    </Grid>
                  </Left>
                  <Body />
                  <Right>
                    <Grid>
                      <Row>
                        <Button transparent onPress={() => this.addWishList()}>
                          <Thumbnail
                            square
                            small
                            source={this.state.showWishList}
                          />
                        </Button>
                      </Row>
                    </Grid>
                  </Right>
                </CardItem>
                <CardItem>
                  <Body>
                    <Text>{item.product_name}</Text>
                  </Body>
                </CardItem>

                <CardItem />
              </Card>
            );
          })}

          <Card>
            <CardItem bordered>
              <Body>
                <Text style={style.titleText}>
                  Buy over $30 for free delivery
                </Text>
              </Body>
            </CardItem>
            <CardItem bordered footer>
              <Text style={style.titleText}>60-Day Buyer Protection</Text>
            </CardItem>
          </Card>

          <Card>
            <CardItem bordered>
              <Body>
                <Text style={style.titleText}>Color</Text>
              </Body>
              <Right>
                <TouchableOpacity onPress={() => this.addToCart()}>
                  <Text style={{ color: "red" }}>Select</Text>
                </TouchableOpacity>
              </Right>
            </CardItem>
            <CardItem>
              <Body>
                <Grid>
                  <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    {this.state.itemColor.length > 0
                      ? this.state.itemColor.map((item, i) => {
                          return (
                            <Col key={i}>
                              <TouchableOpacity
                                onPress={() => this.addToCart()}
                              >
                                <Image
                                  source={{
                                    uri:
                                      sellerUrl + "images/product/" + item.image
                                  }}
                                  style={style.imageColor}
                                />
                              </TouchableOpacity>
                            </Col>
                          );
                        })
                      : null}
                  </ScrollView>
                </Grid>
              </Body>
            </CardItem>
            <CardItem footer />
          </Card>

          <Card>
            <CardItem bordered>
              <Body>
                <Text style={style.titleText}>Item Description</Text>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <FlatList
                  data={this.state.itemDesc}
                  renderItem={({ item }) => (
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <Image
                        source={{
                          uri:
                            sellerUrl +
                            "images/product/" +
                            item.product_description_photo
                        }}
                        style={style.imageDesc}
                      />
                    </View>
                  )}
                  keyExtractor={(item, index) => item.product_description_id}
                />
              </Body>
            </CardItem>
            <CardItem footer />
          </Card>

          <Card>
            <CardItem>
              <Body>
                <Text style={style.titleText}>More To Love</Text>
              </Body>
            </CardItem>
          </Card>
          {this.state.itemSimilar.length > 0 ? (
            <Grid>
              <Row>
                <FlatList
                  data={this.state.itemSimilar}
                  contentContainerStyle={{ flexGrow: 1 }}
                  renderItem={({ item }) => (
                    <View
                      style={{
                        flex: 1 / 2
                      }}
                    >
                      <Card style={{ borderRadius: 10 }}>
                        <CardItem
                          cardBody
                          style={{ alignSelf: "center", borderRadius: 10 }}
                          button
                          onPress={() =>
                            this.goToScreen(item.product_id, "Product")
                          }
                        >
                          <Image
                            source={{
                              uri:
                                sellerUrl +
                                "images/product/" +
                                item.product_photo
                            }}
                            style={style.itemImage}
                          />
                        </CardItem>

                        <CardItem
                          style={{ height: 50 }}
                          button
                          onPress={() =>
                            this.goToScreen(item.product_id, "Product")
                          }
                        >
                          <Body>
                            <Text style={style.itemText}>
                              {item.product_name.slice(0, 35) +
                                (item.product_name.length > 35 ? "..." : "")}
                            </Text>
                          </Body>
                        </CardItem>

                        <CardItem
                          footer
                          style={{
                            borderBottomLeftRadius: 10,
                            borderBottomRightRadius: 10
                          }}
                          button
                          onPress={() =>
                            this.goToScreen(item.product_id, "Product")
                          }
                        >
                          <Left>
                            <Text style={style.itemPrice}>
                              US ${item.promotion_price}
                            </Text>
                          </Left>
                        </CardItem>
                      </Card>
                    </View>
                  )}
                  keyExtractor={(item, index) => item.product_id}
                  numColumns={2}
                />
              </Row>
            </Grid>
          ) : null}

          <ActivityIndicator size="large" color="red" />
        </Content>

        <Footer transparent>
          <FooterTab>
            <Button style={style.addToCart} onPress={() => this.addToCart()}>
              <Text style={style.addToCartText}>ADD TO CART</Text>
            </Button>
            <Button style={style.buyNow} onPress={() => this.buyNow()}>
              <Text style={style.buyNowText}>BUY NOW</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    countItem: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    countCartReducer: count => {
      dispatch({
        type: "COUNT_CART",
        payload: { count: count }
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);

const style = StyleSheet.create({
  overlayContainer: {
    flex: 1
  },
  top: {
    backgroundColor: "rgba(0,0,0,0.2)"
  },

  itemImage: {
    width: "100%",
    height: "100%",
    aspectRatio: 1,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8
  },
  itemPrice: {
    fontWeight: "bold",
    fontSize: 12,
    color: "black"
  },
  itemText: {
    fontWeight: "bold",
    fontSize: 10,
    color: "black"
  },
  afterDiscount: {
    fontSize: 15,
    color: "#808080",
    textDecorationLine: "line-through",
    marginRight: 5
  },
  titleText: {
    fontWeight: "500",
    fontSize: 18,
    color: "black"
  },
  imageColor: {
    borderColor: "#ddd",
    borderWidth: 1,
    height: 64,
    width: 64,
    aspectRatio: 1,
    resizeMode: "stretch",
    borderRadius: 8,
    padding: 10,
    marginRight: 5
  },
  imageDesc: {
    height: undefined,
    width: "100%",
    aspectRatio: 1,
    resizeMode: "stretch"
  },
  addToCart: {
    backgroundColor: "#fff1f1"
  },
  buyNow: {
    backgroundColor: "#ff4747"
  },
  addToCartText: {
    fontSize: 15,
    fontWeight: "500",
    color: "#ff4747"
  },
  buyNowText: {
    fontSize: 15,
    fontWeight: "500",
    color: "white"
  }
});
