import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Title,
  Text,
  Content,
  Card,
  CardItem,
  Footer,
  FooterTab,
  Button
} from "native-base";

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  ToastAndroid
} from "react-native";

import { Navigation } from "react-native-navigation";

import { siteUrl, sellerUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

import NumericInput from "react-native-numeric-input";

import { connect } from "react-redux";

class ProductOption extends Component {
  mounted = false;
  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      itemSize: [],
      size: 0,
      quantity: 1
    };

    this.navigationEventListener = Navigation.events().bindComponent(this);
  }

  componentWillUnmount() {
    this.mounted = false;
    if (this.navigationEventListener) {
      this.navigationEventListener.remove();
    }
  }

  responseProductSize = id => {
    const params = {
      id: id
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseProductSize",
      params
    ).then(json => {
      this.setState({ itemSize: json });
    });
  };

  async _handlingChangeActive(size) {
    await this.setState({ size: size });
  }

  backToScreen = () => {
    Navigation.pop(this.props.componentId);
  };

  _handlingChangeID = id => {
    this.setState({ id: id });
    this.responseProductSize(id);
  };

  continue = () => {
    var isID = false;
    var isSize = false;

    if (this.state.id == 0) {
      ToastAndroid.show("Please select color of item !", ToastAndroid.SHORT);
      isID = false;
    } else {
      isID = true;
    }

    if (this.state.itemSize.length > 0) {
      if (this.state.size == 0) {
        ToastAndroid.show("Please select size of item !", ToastAndroid.SHORT);
        isSize = false;
      } else {
        isSize = true;
      }
    } else {
      isSize = true;
    }

    if (isID && isSize) {
      if (this.props.user == 0) {
        Navigation.push(this.props.componentId, {
          component: {
            name: "JoinOrSignIn",
            passProps: {
              proID: this.state.id,
              qty: this.state.quantity,
              price: this.props.price,
              status: this.props.status
            },
            options: {
              topBar: {
                visible: false,
                drawBehind: true,
                animate: false
              },
              bottomTabs: {
                visible: false,
                drawBehind: true,
                animate: false
              },
              animations: {
                setStackRoot: {
                  enabled: true
                }
              }
            }
          }
        });
      } else {
        const params = {
          id: this.state.id,
          qty: this.state.quantity,
          price: this.props.price,
          user_id: this.props.user
        };

        ServerClass.responsePostData(
          siteUrl + "mobileApi/addToCart",
          params
        ).then(json => {
          if (json.statusCode == "S0001") {
            this.props.addToCart(
              this.state.id,
              this.state.quantity,
              this.props.price
            );
            this.props.countCartReducer(json.count);
            ToastAndroid.show(
              "Item has been added successful ! ",
              ToastAndroid.SHORT
            );
          }
        });

        if (this.props.status == "add") {
          Navigation.pop(this.props.componentId);
        } else {
          Navigation.push(this.props.componentId, {
            component: {
              name: "ViewCart",
              passProps: {
                user: this.props.user
              },
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                bottomTabs: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                animations: {
                  setStackRoot: {
                    enabled: true
                  }
                }
              }
            }
          });
        }
      }
    }
  };

  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header style={{ backgroundColor: "black" }}>
          <Left>
            <Button transparent onPress={() => this.backToScreen()}>
              <Icon style={style.headerIcon} name="arrow-round-back" />
            </Button>
          </Left>
          <Body>
            <Title>Product Option</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Card transparent>
            {this.props.itemData.map((item, i) => {
              return (
                <CardItem bordered key={i}>
                  <Left>
                    <Image
                      source={{
                        uri: sellerUrl + "images/product/" + item.product_photo
                      }}
                      style={style.itemImage}
                    />
                  </Left>
                  <Body>
                    <CardItem>
                      <Body>
                        <Text>
                          {item.product_name.slice(0, 50) +
                            (item.product_name.length > 50 ? "..." : "")}
                        </Text>
                      </Body>
                    </CardItem>
                    <CardItem>
                      <Body>
                        <View style={{ flexDirection: "row" }}>
                          <Text style={style.itemPrice}>
                            US ${item.promotion_price} / {item.measure_name}
                          </Text>
                        </View>
                      </Body>
                    </CardItem>
                  </Body>
                </CardItem>
              );
            })}

            <CardItem>
              <Body>
                <Text style={{ marginBottom: 10 }}>Color : Please Select</Text>
                <FlatList
                  data={this.props.itemColor}
                  renderItem={({ item, i }) => (
                    <TouchableOpacity
                      onPress={() => this._handlingChangeID(item.detail_id)}
                    >
                      <Image
                        source={{
                          uri: sellerUrl + "images/product/" + item.image
                        }}
                        style={
                          this.state.id == item.detail_id
                            ? style.activeImageColor
                            : style.unActiveImageColor
                        }
                      />
                    </TouchableOpacity>
                  )}
                  keyExtractor={(item, index) => item.color_id}
                  numColumns={4}
                  ListFooterComponent={() => (
                    <View>
                      {this.state.itemSize.map((item, i) => {
                        return (
                          <View key={i}>
                            <Text style={{ marginBottom: 10 }}>
                              Size : Please Select
                            </Text>
                            <TouchableOpacity
                              style={
                                this.state.size == item.product_size_id
                                  ? style.activeButtonSize
                                  : style.unActiveButtonSize
                              }
                              onPress={() =>
                                this._handlingChangeActive(item.product_size_id)
                              }
                            >
                              <Text style={{ textAlign: "center" }}>
                                {item.size_name}
                              </Text>
                            </TouchableOpacity>
                          </View>
                        );
                      })}
                    </View>
                  )}
                />
              </Body>
            </CardItem>

            <CardItem bordered>
              <Body>
                <Text style={{ marginBottom: 10 }}>Quanity</Text>

                <NumericInput
                  value={this.state.quantity}
                  onChange={value => this.setState({ quantity: value })}
                  totalWidth={150}
                  totalHeight={35}
                  iconSize={15}
                  step={1}
                  valueType="real"
                  rounded
                  textColor="#ff4747"
                  iconStyle={{ color: "black" }}
                  rightButtonBackgroundColor="#ddd"
                  leftButtonBackgroundColor="#ddd"
                  minValue={Number(1)}
                />
              </Body>
            </CardItem>
          </Card>
        </Content>
        <Footer transparent>
          <FooterTab>
            <Button
              style={style.continueButton}
              onPress={() => this.continue()}
            >
              <Text style={style.continueButtonText}>CONTINUE</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addToCart: (id, qty, price) => {
      dispatch({
        type: "ADD_TO_CART",
        payload: { id: id, qty: qty, price: price }
      });
    },
    countCartReducer: count => {
      dispatch({
        type: "COUNT_CART",
        payload: { count: count }
      });
    }
  };
};
export default connect(null, mapDispatchToProps)(ProductOption);

const style = StyleSheet.create({
  headerIcon: {
    color: "white"
  },
  itemImage: {
    height: 160,
    width: 160,
    aspectRatio: 1,
    resizeMode: "stretch",
    borderRadius: 8
  },
  itemPrice: {
    fontWeight: "500",
    color: "black",
    paddingLeft: 0,
    justifyContent: "center"
  },

  activeImageColor: {
    height: 75,
    width: 75,
    aspectRatio: 1,
    resizeMode: "stretch",
    borderRadius: 8,
    marginRight: 5,
    marginBottom: 5,
    borderColor: "red",
    borderWidth: 2,
    borderRadius: 5
  },
  unActiveImageColor: {
    height: 75,
    width: 75,
    aspectRatio: 1,
    resizeMode: "stretch",
    borderRadius: 8,
    marginRight: 5,
    marginBottom: 5,
    borderColor: "#ddd",
    borderWidth: 2,
    borderRadius: 10
  },
  activeButtonSize: {
    color: "black",
    borderColor: "red",
    borderWidth: 2,
    borderRadius: 5,
    padding: 10
  },
  unActiveButtonSize: {
    color: "black",
    borderColor: "#ddd",
    borderWidth: 2,
    borderRadius: 5,
    padding: 10
  },
  continueButton: {
    backgroundColor: "#ff4747"
  },
  continueButtonText: {
    fontSize: 15,
    fontWeight: "500",
    color: "white"
  }
});
