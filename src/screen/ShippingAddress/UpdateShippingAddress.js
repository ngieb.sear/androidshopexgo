import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Icon,
  Text,
  Content,
  Title,
  Item,
  Input,
  Card,
  CardItem,
  Picker,
  Switch
} from "native-base";

import {
  StyleSheet,
  View,
  ToastAndroid,
  ActivityIndicator
} from "react-native";

import { siteUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

import { Navigation } from "react-native-navigation";

import { Grid, Col } from "react-native-easy-grid";

class UpdateShippingAddress extends Component {
  mounted = false;
  constructor(props) {
    super(props);
    this.state = {
      isFalse: false,
      isLoading: false,
      isDefault: false,
      chosenDate: new Date(),
      contactCode: "",
      mobile: "",
      deliveryAddress: "",
      province: "",
      city: "",
      user_id: 0,
      countryCode: "KH",
      country: []
    };
  }

  componentDidMount() {
    this.mounted = true;
    this.responseCountry();
  }

  backToScreen = () => {
    Navigation.pop(this.props.componentId);
  };

  responseUpdateData = () => {
    const params = {
      id: this.props.id
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/UpdateShippingAddress",
      params
    ).then(json => {
      this.setState({
        mobile: json.shipping_phone,
        province: json.shipping_state,
        city: json.shipping_city,
        countryCode: json.shipping_country,
        deliveryAddress: json.shipping_address,
        isDefault: json.is_default
      });
    });
  };

  saveShipping = () => {
    var isFalse = false;
    if (this.state.mobile == "") {
      ToastAndroid.show("Please enter your Phone Number !", ToastAndroid.SHORT);
    } else if (this.state.province == "") {
      ToastAndroid.show(
        "Please enter your State/Province/County !",
        ToastAndroid.SHORT
      );
    } else if (this.state.city == "") {
      ToastAndroid.show("Please enter your City !", ToastAndroid.SHORT);
    } else if (this.state.deliveryAddress == "") {
      ToastAndroid.show("Please enter your Address !", ToastAndroid.SHORT);
    } else {
      isFalse = true;
      this.setState({ isLoading: true });
    }

    if (isFalse) {
      const params = {
        id: this.props.id,
        address: this.state.deliveryAddress,
        phone: this.state.mobile,
        province: this.state.province,
        city: this.state.city,
        countryCode: this.state.countryCode,
        user_id: this.props.user,
        default: this.state.isDefault
      };

      ServerClass.responsePostData(
        siteUrl + "mobileApi/saveShippingSetting",
        params
      ).then(json => {
        if (json.statusCode == "S0001") {
          Navigation.pop(this.props.componentId);
        }
      });
    }
  };

  responseCountry = () => {
    if (this.mounted) {
      ServerClass.responsGetData(siteUrl + "mobileApi/responseCountry").then(
        json => {
          this.setState({ country: json });
          this.responseUpdateData();
        }
      );
    }
  };

  render() {
    return (
      <Container style={{ backgroundColor: "white" }}>
        <Header noShadow style={{ backgroundColor: "black" }}>
          <Left>
            <Button transparent onPress={() => this.backToScreen()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Update Address</Title>
          </Body>
        </Header>
        <Content padder>
          <Card>
            <CardItem>
              <Body>
                <Text style={{ fontSize: 12 }}>
                  Please enter your address in English as required
                </Text>
              </Body>
            </CardItem>
          </Card>

          <Grid>
            <Col>
              <Item>
                <Input
                  placeholder="Mobile"
                  placeholderTextColor="#cccccc"
                  onChangeText={val => this.setState({ mobile: val })}
                  value={this.state.mobile}
                />
              </Item>
            </Col>
          </Grid>
          <Item>
            <Input
              placeholder="Delivery Address"
              placeholderTextColor="#cccccc"
              onChangeText={val => this.setState({ deliveryAddress: val })}
              value={this.state.deliveryAddress}
            />
          </Item>
          <Item picker style={{ marginTop: 10 }}>
            <Picker
              renderHeader={backAction => (
                <Header style={{ backgroundColor: "#ff4747" }}>
                  <Left>
                    <Button transparent onPress={backAction}>
                      <Icon name="arrow-back" style={{ color: "#fff" }} />
                    </Button>
                  </Left>
                  <Body style={{ flex: 3 }}>
                    <Title style={{ color: "#fff" }}>Choose your country</Title>
                  </Body>
                  <Right />
                </Header>
              )}
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              selectedValue={this.state.countryCode}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ countryCode: itemValue })
              }
            >
              {this.state.country.map(val => {
                return (
                  <Picker.Item
                    key={"country-item-" + val.country_id}
                    label={val.country_name}
                    value={val.iso2}
                  />
                );
              })}
            </Picker>
          </Item>
          <Item>
            <Input
              placeholder="State/Province/County"
              placeholderTextColor="#cccccc"
              onChangeText={val => this.setState({ province: val })}
              value={this.state.province}
            />
          </Item>
          <Item>
            <Input
              placeholder="City"
              placeholderTextColor="#cccccc"
              onChangeText={val => this.setState({ city: val })}
              value={this.state.city}
            />
          </Item>
          <View style={{ marginTop: 10 }}>
            <Grid>
              <Col size={2}>
                <Text>Set as default shipping address</Text>
              </Col>
              <Col size={1}>
                <Switch
                  value={this.state.isDefault}
                  onValueChange={val => this.setState({ isDefault: val })}
                ></Switch>
              </Col>
            </Grid>
          </View>
          <View style={{ marginTop: 20 }}>
            <Button style={style.buy} onPress={() => this.saveShipping()}>
              <Text style={style.buyText}>Save</Text>
            </Button>
          </View>
        </Content>
        {this.state.isLoading ? (
          <View style={style.spinnerBackground}>
            <View style={style.spinnerView}>
              <ActivityIndicator size="large" color="red" />
            </View>
          </View>
        ) : null}
      </Container>
    );
  }
}

export default UpdateShippingAddress;

const style = StyleSheet.create({
  buy: {
    backgroundColor: "#ff4747",
    width: "100%",
    borderRadius: 5,
    alignContent: "center",
    justifyContent: "center"
  },
  buyText: {
    fontSize: 15,
    fontWeight: "500",
    color: "white",
    alignSelf: "center"
  },
  spinnerView: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  spinnerBackground: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.5)"
  }
});
