import React, { Component } from "react";

import { View, Text } from "react-native";

import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Icon,
  Left,
  Right,
  Body,
  Radio,
  Button,
  Title
} from "native-base";

import { Navigation } from "react-native-navigation";

import { siteUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

class ShippingAddress extends Component {
  mounted = false;
  constructor(props) {
    super(props);

    this.state = {
      shippingData: [],
      user: 0,
      isDefault: []
    };
  }

  componentDidMount = () => {
    this.mounted = true;
    this.responseShipping();
  };

  componentWillUnmount() {
    this.mounted = false;
  }

  responseShipping = () => {
    if (this.props.user > 0) {
      const params = {
        user_id: this.props.user
      };

      if (this.mounted) {
        ServerClass.responsePostData(
          siteUrl + "mobileApi/responseShippingData",
          params
        ).then(json => {
          var shippingData = [];
          var is_default = [];
          for (i = 0; i < json.length; i++) {
            if (json[i].is_default == 1) {
              is_default = true;
            } else {
              is_default = false;
            }
            shippingData.push({
              user_name: json[i].user_name,
              shipping_setting_id: json[i].shipping_setting_id,
              shipping_email: json[i].shipping_email,
              shipping_phone: json[i].shipping_phone,
              shipping_address: json[i].shipping_address,
              shipping_state: json[i].shipping_state,
              shipping_city: json[i].shipping_city,
              shipping_country: json[i].shipping_country,
              is_default: is_default
            });
          }
          this.setState({ shippingData: shippingData });
        });
      }
    }
  };

  goToScreen = screen => {
    if (screen == "back") {
      Navigation.pop(this.props.componentId);
    } else if (screen == "add") {
      Navigation.push(this.props.componentId, {
        component: {
          name: "AddNewShippingAddress",
          passProps: {
            user: this.props.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    }
  };

  updateDefault = id => {
    const params = {
      id: id,
      user: this.props.user
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/updateShippingDefault",
      params
    ).then(json => {
      if (json.statusCode == "S0001") {
        this.responseShipping();
      }
    });
  };

  removeAddress = id => {
    const params = {
      id: id,
      user: this.props.user
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/removeShippingSetting",
      params
    ).then(json => {
      if (json.statusCode == "S0001") {
        this.responseShipping();
      }
    });
  };

  updateAddress = id => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "UpdateShippingAddress",
        passProps: {
          id: id,
          user: this.props.user
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };
  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header searchBar rounded noShadow style={{ backgroundColor: "#000" }}>
          <Left>
            <Icon
              name="arrow-back"
              style={{ color: "white" }}
              onPress={() => this.goToScreen("back")}
            />
          </Left>
          <Body>
            <Title>Shipping Address</Title>
          </Body>

          <Right>
            <Button transparent onPress={() => this.responseShipping()}>
              <Icon name="refresh" />
            </Button>
            <Button transparent onPress={() => this.goToScreen("add")}>
              <Icon name="add" />
            </Button>
          </Right>
        </Header>
        <Content>
          {this.state.shippingData.map(item => {
            return (
              <Card transparent key={item.shipping_setting_id}>
                <CardItem>
                  <Body>
                    <View>
                      <Title style={{ color: "black" }}>{item.user_name}</Title>
                    </View>
                    <View>
                      <Text>{item.shipping_address}</Text>
                    </View>
                    <View>
                      <Text>{item.shipping_state}</Text>
                    </View>

                    <View>
                      <Text>
                        {item.shipping_city},{item.shipping_country}
                      </Text>
                    </View>

                    <View>
                      <Text>{item.shipping_phone}</Text>
                    </View>
                  </Body>
                  <Right>
                    <View>
                      <Radio
                        color={"#f0ad4e"}
                        selectedColor={"#5cb85c"}
                        selected={item.is_default}
                        onPress={() =>
                          this.updateDefault(item.shipping_setting_id)
                        }
                      />
                    </View>
                  </Right>
                </CardItem>
                <CardItem>
                  <Left>
                    {item.is_default ? (
                      <View
                        style={{
                          backgroundColor: "#ff4747",
                          padding: 10,
                          borderRadius: 8
                        }}
                      >
                        <Text style={{ color: "white" }}>Default</Text>
                      </View>
                    ) : null}
                  </Left>
                  <Body></Body>
                  <Right>
                    <View style={{ flexDirection: "row" }}>
                      <Button
                        transparent
                        style={{ marginRight: 10 }}
                        onPress={() =>
                          this.updateAddress(item.shipping_setting_id)
                        }
                      >
                        <Text style={{ color: "#ff4747" }}>Edit</Text>
                      </Button>
                      {item.is_default ? null : (
                        <Button
                          transparent
                          onPress={() =>
                            this.removeAddress(item.shipping_setting_id)
                          }
                        >
                          <Text style={{ color: "#ff4747" }}>Delete</Text>
                        </Button>
                      )}
                    </View>
                  </Right>
                </CardItem>
              </Card>
            );
          })}
        </Content>
      </Container>
    );
  }
}

export default ShippingAddress;
