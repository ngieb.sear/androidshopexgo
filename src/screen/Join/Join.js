import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Text,
  Thumbnail,
  Content,
  Title,
  Item,
  DatePicker
} from "native-base";

import {
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Dimensions,
  ToastAndroid,
  ActivityIndicator
} from "react-native";

import moment from "moment";

import { siteUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

import { Navigation } from "react-native-navigation";

import { connect } from "react-redux";

import {
  LoginManager,
  GraphRequest,
  GraphRequestManager
} from "react-native-fbsdk";

import {
  GoogleSignin,
  statusCodes
} from "@react-native-community/google-signin";

const { width: screenWidth } = Dimensions.get("window");
const { height: screenHeight } = Dimensions.get("window");

class Join extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFalse: false,
      isLoading: false,
      chosenDate: new Date(),
      username: "",
      password: "",
      confirm: "",
      first_name: "",
      last_name: "",
      email: "",
      user_id: 0
    };
  }

  async register() {
    if (this.state.email == "" || this.state.email != "") {
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (reg.test(this.state.email) === false) {
        ToastAndroid.show("Your email is not valid !", ToastAndroid.SHORT);
      } else if (this.state.first_name == "") {
        ToastAndroid.show("Please enter your First Name !", ToastAndroid.SHORT);
      } else if (this.state.last_name == "") {
        ToastAndroid.show("Please enter your Last Name !", ToastAndroid.SHORT);
      } else if (this.state.chosenDate == "") {
        ToastAndroid.show(
          "Please select your Date of Birth !",
          ToastAndroid.SHORT
        );
      } else if (this.state.password == "") {
        ToastAndroid.show("Please enter your Password !", ToastAndroid.SHORT);
      } else if (this.state.confirm == "") {
        ToastAndroid.show(
          "Please enter your Confirm Password !",
          ToastAndroid.SHORT
        );
      } else {
        await this.setState({ isFalse: true });
      }
    }

    if (this.state.isFalse) {
      this.setState({ isLoading: true });
      const params = {
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        create_password: this.state.password,
        confirm_password: this.state.confirm,
        email_account: this.state.email,
        dateofbirth: this.state.chosenDate
      };

      ServerClass.responsePostData(siteUrl + "mobileApi/register", params).then(
        json => {
          if (json.statusCode == "S0001") {
            ServerClass.setUser(json.user_id);
            this.props.setUser(json.user_id);
            this.setState({ user_id: json.user_id });
            if (this.props.status == "wishlist") {
              this.addWishList();
            } else if (this.props.status == "add") {
              this.addItemToCart();
            } else if (this.props.status == "buy") {
              this.addItemToCart();
            } else {
            }
          } else {
            ToastAndroid.show(json.msg, ToastAndroid.SHORT);
          }

          Navigation.pop(this.props.componentId);
          Navigation.pop(this.props.comId);
        }
      );
    }
  }

  componentDidMount() {
    this.configureGoogleSignIn();
  }

  configureGoogleSignIn() {
    GoogleSignin.configure({
      webClientId:
        "131982394714-ri9vo1fglua796ikigm7t1rlp4jp9lsk.apps.googleusercontent.com",
      offlineAccess: false
    });
  }

  loginGoogle = async () => {
    this.props.addToCart;
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      if (userInfo.user_id != "") {
        const params = {
          g_id: userInfo.user.id,
          first_name: userInfo.user.familyName,
          last_name: userInfo.user.givenName,
          email: userInfo.user.email
        };
        ServerClass.responsePostData(
          siteUrl + "mobileApi/registerGoogle",
          params
        ).then(json => {
          if (json.statusCode == "S0001") {
            ServerClass.setUser(json.user_id);
            this.setState({ user_id: json.user_id });
            this.props.setUser(json.user_id);
            if (this.props.status == "wishlist") {
              this.addWishList();
            } else if (this.props.status == "add") {
              this.addItemToCart();
            } else if (this.props.status == "buy") {
              this.buyNow();
            } else {
              Navigation.pop(this.props.componentId);
              Navigation.pop(this.props.comId);
            }
          }
        });
      }
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.warn("user cancelled the login flow");
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.warn("operation (f.e. sign in) is in progress already");
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.warn("play services not available or outdated");
      } else {
        console.warn(error);
      }
    }
  };

  async loginFacebook() {
    try {
      let result = await LoginManager.logInWithPermissions(["public_profile"]);
      if (result.isCancelled) {
        alert("Login was cancelled");
      } else {
        const infoRequest = new GraphRequest(
          "/me?fields=id,first_name,last_name,picture.type(large)",
          null,
          this._responseInfoCallback
        );
        new GraphRequestManager().addRequest(infoRequest).start();
      }
    } catch (error) {
      ToastAndroid.show("An error occured : " + error, ToastAndroid.SHORT);
    }
  }

  _responseInfoCallback = (error, result) => {
    if (error) {
      ToastAndroid.show(
        "Error fetching data: " + error.toString(),
        ToastAndroid.SHORT
      );
    } else {
      const params = {
        fb_id: result.id,
        first_name: result.first_name,
        last_name: result.last_name
      };

      ServerClass.responsePostData(
        siteUrl + "mobileApi/registerFB",
        params
      ).then(json => {
        if (json.statusCode == "S0001") {
          ServerClass.setUser(json.user_id);
          this.props.setUser(json.user_id);
          this.setState({ user_id: json.user_id });
          if (this.props.status == "wishlist") {
            this.addWishList();
          } else if (this.props.status == "add") {
            this.addItemToCart();
          } else if (this.props.status == "buy") {
            this.buyNow();
          } else {
            Navigation.pop(this.props.componentId);
            Navigation.pop(this.props.comId);
          }
        }
      });
    }
  };

  backToScreen = () => {
    Navigation.pop(this.props.componentId);
  };

  addItemToCart = () => {
    const params = {
      id: this.props.proID,
      price: this.props.price,
      qty: this.props.qty,
      user_id: this.state.user_id
    };
    ServerClass.responsePostData(siteUrl + "mobileApi/addToCart", params).then(
      json => {
        if (json.statusCode == "S0001") {
          this.props.addToCart(
            this.props.proID,
            this.props.qty,
            this.props.price
          );
          this.props.countCartReducer(json.count);
          ToastAndroid.show(
            "Item has been added successful ! ",
            ToastAndroid.SHORT
          );

          Navigation.pop(this.props.componentId);

          Navigation.pop(this.props.comId);
        }
      }
    );
  };

  addWishList = () => {
    const params = {
      id: this.props.proID,
      user: this.state.user_id
    };
    ServerClass.responsePostData(
      siteUrl + "mobileApi/addWishList",
      params
    ).then(json => {
      if (json.statusCode == "E0002") {
        ToastAndroid.show(
          "This item is already in your wishlist !",
          ToastAndroid.SHORT
        );
      } else {
        ToastAndroid.show("Added !", ToastAndroid.SHORT);

        Navigation.pop(this.props.componentId);
      }
    });
  };

  buyNow = () => {
    const params = {
      id: this.props.proID,
      price: this.props.price,
      qty: this.props.qty,
      user_id: this.state.user_id
    };
    ServerClass.responsePostData(siteUrl + "mobileApi/addToCart", params).then(
      json => {
        if (json.statusCode == "S0001") {
          this.props.addToCart(
            this.props.proID,
            this.props.qty,
            this.props.price
          );
          this.props.countCartReducer(json.count);

          ToastAndroid.show(
            "Item has been added successful ! ",
            ToastAndroid.SHORT
          );

          Navigation.push(this.props.componentId, {
            component: {
              name: "ViewCart",
              passProps: {
                user: this.state.user_id
              },
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                bottomTabs: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                animations: {
                  setStackRoot: {
                    enabled: true
                  }
                }
              }
            }
          });
        }
      }
    );
  };

  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header noShadow style={{ backgroundColor: "#ff4747" }}>
          <Left>
            <TouchableOpacity onPress={() => this.backToScreen()}>
              <Icon style={style.headerIcon} name="arrow-round-back" />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title>Register</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <View style={{ height: screenHeight - 55 }}>
            <View style={{ padding: 10 }}>
              <TextInput
                style={style.textInput}
                placeholder={"Email Account"}
                keyboardType="email-address"
                onChangeText={text => {
                  this.setState({ email: text });
                }}
              />
              <TextInput
                style={style.textInput}
                placeholder={"First Name"}
                onChangeText={text => {
                  this.setState({ first_name: text });
                }}
              />
              <TextInput
                style={style.textInput}
                placeholder={"Last Name"}
                onChangeText={text => {
                  this.setState({ last_name: text });
                }}
              />
              <Item style={style.textInput}>
                <DatePicker
                  defaultDate={new Date()}
                  maximumDate={new Date()}
                  locale={"kh"}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={"fade"}
                  androidMode={"spinner"}
                  placeHolderText="Select Birth of Date"
                  textStyle={{
                    color: "black",
                    fontSize: 16,
                    fontWeight: "500"
                  }}
                  placeHolderTextStyle={{
                    color: "black",
                    fontSize: 16,
                    fontWeight: "500"
                  }}
                  onDateChange={date => {
                    this.setState({
                      chosenDate: moment(date).format("YYYY-MM-DD")
                    });
                  }}
                  disabled={false}
                />
              </Item>

              <TextInput
                style={style.textInput}
                placeholder={"Password"}
                secureTextEntry={true}
                onChangeText={text => {
                  this.setState({ password: text });
                }}
              />
              <TextInput
                style={style.textInput}
                placeholder={"Confirm Password"}
                secureTextEntry={true}
                onChangeText={text => {
                  this.setState({ confirm: text });
                }}
              />

              <Button
                block
                style={style.register}
                onPress={() => this.register()}
              >
                <Text style={style.registerText}>AGREE & CREATE ACCOUNT</Text>
              </Button>
            </View>

            <View style={{ padding: 10 }}>
              <Text style={{ textAlign: "center" }}>Or Sign up with</Text>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{
                  flexGrow: 1,
                  justifyContent: "center"
                }}
              >
                {/* {this.state.signUpWith.map((item, i) => {
                  return (
                    
                  );
                })} */}
                <TouchableOpacity onPress={() => this.loginGoogle()}>
                  <Thumbnail
                    square
                    large
                    source={require("../../assets/icon/google.png")}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.loginFacebook()}>
                  <Thumbnail
                    square
                    large
                    source={require("../../assets/icon/facebook.png")}
                  />
                </TouchableOpacity>
              </ScrollView>
            </View>

            {this.state.isLoading ? (
              <View style={style.spinnerBackground}>
                <View style={style.spinnerView}>
                  <ActivityIndicator size="large" color="red" />
                </View>
              </View>
            ) : null}
          </View>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addToCart: (id, qty, price) => {
      dispatch({
        type: "ADD_TO_CART",
        payload: { id: id, qty: qty, price: price }
      });
    },
    countCartReducer: count => {
      dispatch({
        type: "COUNT_CART",
        payload: { count: count }
      });
    },
    setUser: user => {
      dispatch({
        type: "SET_USER",
        payload: { user: user }
      });
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Join);

const style = StyleSheet.create({
  headerIcon: {
    color: "white"
  },
  textInput: {
    borderRadius: 5,
    width: screenWidth - 20,
    height: 45,
    fontSize: 16,
    marginVertical: 10,
    backgroundColor: "#ddd",
    color: "black"
  },
  register: {
    backgroundColor: "#ff4747",
    marginTop: 10
  },
  registerText: {
    fontSize: 18,
    fontWeight: "500",
    color: "white",
    textAlign: "center"
  },
  spinnerView: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  spinnerBackground: {
    height: screenHeight,
    width: screenWidth,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,0.2)"
  }
});
