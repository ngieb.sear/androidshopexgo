import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Text,
  Thumbnail,
  Content,
  Title
} from "native-base";

import {
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Dimensions,
  ToastAndroid,
  ActivityIndicator
} from "react-native";

import { siteUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

import { connect } from "react-redux";

import { Navigation } from "react-native-navigation";

import {
  LoginManager,
  GraphRequest,
  GraphRequestManager
} from "react-native-fbsdk";

import {
  GoogleSignin,
  statusCodes
} from "@react-native-community/google-signin";

const { width: screenWidth } = Dimensions.get("window");
const { height: screenHeight } = Dimensions.get("window");

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFalse: false,
      isLoading: false,
      password: "",
      email: "",
      signInWith: [
        {
          id: 1,
          icon: require("../../assets/icon/google.png")
        },
        {
          id: 2,
          icon: require("../../assets/icon/facebook.png")
        },
        {
          id: 3,
          icon: require("../../assets/icon/twitter.png")
        }
      ],
      user_id: 0
    };
  }

  componentDidMount() {
    this.configureGoogleSignIn();
  }

  async signIn() {
    if (this.state.email == "" || this.state.email != "") {
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (reg.test(this.state.email) === false) {
        ToastAndroid.show("Your email is not valid !", ToastAndroid.SHORT);
      } else if (this.state.password == "") {
        ToastAndroid.show("Please enter your Password !", ToastAndroid.SHORT);
      } else {
        await this.setState({ isFalse: true });
      }
    }

    if (this.state.isFalse) {
      this.setState({ isLoading: true });
      const params = {
        password: this.state.password,
        email_account: this.state.email
      };

      ServerClass.responsePostData(siteUrl + "mobileApi/signIn", params).then(
        json => {
          if (json.statusCode == "S0001") {
            this.setState({ user_id: json.user_id });
            if (this.props.status == "wishlist") {
              this.addWishList();
            } else if (this.props.status == "addCart") {
              this.addItemToCart();
            } else {
            }
          } else {
            ToastAndroid.show(json.msg, ToastAndroid.SHORT);
            this.setState({ isLoading: false });
          }
        }
      );
    }
  }

  goToRegister = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "Join",
        passProps: {
          comId: this.props.componentId
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };

  backToScreen = () => {
    Navigation.pop(this.props.componentId);
  };

  configureGoogleSignIn() {
    GoogleSignin.configure({
      webClientId:
        "131982394714-ri9vo1fglua796ikigm7t1rlp4jp9lsk.apps.googleusercontent.com",
      offlineAccess: false
    });
  }

  loginGoogle = async () => {
    this.props.addToCart;
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      const params = {
        g_id: userInfo.user.id,
        first_name: userInfo.user.familyName,
        last_name: userInfo.user.givenName,
        email: userInfo.user.email
      };
      ServerClass.responsePostData(
        siteUrl + "mobileApi/registerGoogle",
        params
      ).then(json => {
        if (json.statusCode == "S0001") {
          ServerClass.setUser(json.user_id);
          this.setState({ user_id: json.user_id });
          this.props.setUser(json.user_id);
          if (this.props.status == "wishlist") {
            this.addWishList();
          } else if (this.props.status == "add") {
            this.addItemToCart();
          } else if (this.props.status == "buy") {
            this.buyNow();
          } else {
            Navigation.pop(this.props.componentId);
            Navigation.pop(this.props.comId);
          }
        }
      });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.warn("user cancelled the login flow");
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.warn("operation (f.e. sign in) is in progress already");
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.warn("play services not available or outdated");
      } else {
        console.warn(error);
      }
    }
  };

  async loginFacebook() {
    try {
      let result = await LoginManager.logInWithPermissions(["public_profile"]);
      if (result.isCancelled) {
        ToastAndroid.show("Login was cancelled", ToastAndroid.SHORT);
      } else {
        const infoRequest = new GraphRequest(
          "/me?fields=id,first_name,last_name,picture.type(large)",
          null,
          this._responseInfoCallback
        );
        new GraphRequestManager().addRequest(infoRequest).start();
      }
    } catch (error) {
      ToastAndroid.show("An error occured : " + error, ToastAndroid.SHORT);
    }
  }

  _responseInfoCallback = (error, result) => {
    if (error) {
      ToastAndroid.show(
        "Error fetching data: " + error.toString(),
        ToastAndroid.SHORT
      );
    } else {
      const params = {
        fb_id: result.id,
        first_name: result.first_name,
        last_name: result.last_name
      };

      ServerClass.responsePostData(
        siteUrl + "mobileApi/registerFB",
        params
      ).then(json => {
        if (json.statusCode == "S0001") {
          ServerClass.setUser(json.user_id);
          this.props.setUser(json.user_id);
          this.setState({ user_id: json.user_id });
          if (this.props.status == "wishlist") {
            this.addWishList();
          } else if (this.props.status == "add") {
            this.addItemToCart();
          } else if (this.props.status == "buy") {
            this.buyNow();
          } else {
            Navigation.pop(this.props.componentId);
            Navigation.pop(this.props.comId);
          }
        }
      });
    }
  };

  addItemToCart = () => {
    const params = {
      id: this.props.proID,
      price: this.props.price,
      qty: this.props.qty,
      user_id: this.state.user_id
    };
    ServerClass.responsePostData(siteUrl + "mobileApi/addToCart", params).then(
      json => {
        if (json.statusCode == "S0001") {
          this.props.addToCart(
            this.props.proID,
            this.props.qty,
            this.props.price
          );
          this.props.countCartReducer(json.count);
          ToastAndroid.show(
            "Item has been added successful ! ",
            ToastAndroid.SHORT
          );

          Navigation.pop(this.props.componentId);

          Navigation.pop(this.props.comId);
        }
      }
    );
  };

  addWishList = () => {
    const params = {
      id: this.props.proID,
      user: this.state.user_id
    };
    ServerClass.responsePostData(
      siteUrl + "mobileApi/addWishList",
      params
    ).then(json => {
      if (json.statusCode == "E0002") {
        ToastAndroid.show(
          "This item is already in your wishlist !",
          ToastAndroid.SHORT
        );
      } else {
        ToastAndroid.show("Added !", ToastAndroid.SHORT);

        Navigation.pop(this.props.componentId);
      }
    });
  };

  buyNow = () => {
    const params = {
      id: this.props.proID,
      price: this.props.price,
      qty: this.props.qty,
      user_id: this.state.user_id
    };
    ServerClass.responsePostData(siteUrl + "mobileApi/addToCart", params).then(
      json => {
        if (json.statusCode == "S0001") {
          this.props.addToCart(
            this.props.proID,
            this.props.qty,
            this.props.price
          );
          this.props.countCartReducer(json.count);

          ToastAndroid.show(
            "Item has been added successful ! ",
            ToastAndroid.SHORT
          );

          Navigation.push(this.props.componentId, {
            component: {
              name: "ViewCart",
              passProps: {
                user: this.state.user_id
              },
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                bottomTabs: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                animations: {
                  setStackRoot: {
                    enabled: true
                  }
                }
              }
            }
          });
        }
      }
    );
  };

  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header noShadow style={{ backgroundColor: "#ff4747" }}>
          <Left>
            <TouchableOpacity onPress={() => this.backToScreen()}>
              <Icon style={style.headerIcon} name="arrow-round-back" />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title>Sign In</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <View style={{ height: screenHeight - 55 }}>
            <View style={{ padding: 10 }}>
              <TextInput
                style={style.textInput}
                placeholder={"Email"}
                onChangeText={text => {
                  this.setState({ email: text });
                }}
              />
              <TextInput
                style={style.textInput}
                secureTextEntry={true}
                placeholder={"Password"}
                onChangeText={text => {
                  this.setState({ password: text });
                }}
              />

              <Button
                block
                style={style.register}
                onPress={() => this.signIn()}
              >
                <Text style={style.registerText}>SIGN IN</Text>
              </Button>
            </View>

            <View>
              <Button transparent style={{ alignSelf: "center" }}>
                <Text style={{ color: "red" }}>Forgot Password ?</Text>
              </Button>
            </View>

            <View style={{ padding: 10 }}>
              <Text style={{ textAlign: "center" }}>Or Sign in with</Text>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{
                  flexGrow: 1,
                  justifyContent: "center"
                }}
              >
                <TouchableOpacity onPress={() => this.loginGoogle()}>
                  <Thumbnail
                    square
                    large
                    source={require("../../assets/icon/google.png")}
                  />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.loginFacebook()}>
                  <Thumbnail
                    square
                    large
                    source={require("../../assets/icon/facebook.png")}
                  />
                </TouchableOpacity>
              </ScrollView>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignContent: "center",
                  justifyContent: "center"
                }}
              >
                <View style={{ marginRight: 10 }}>
                  <Text>No Account ?</Text>
                </View>
                <View>
                  <Text
                    style={{ color: "red" }}
                    onPress={() => this.goToRegister()}
                  >
                    Register
                  </Text>
                </View>
              </View>
            </View>

            {this.state.isLoading ? (
              <View style={style.spinnerBackground}>
                <View style={style.spinnerView}>
                  <ActivityIndicator size="large" color="red" />
                </View>
              </View>
            ) : null}
          </View>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addToCart: (id, qty, price) => {
      dispatch({
        type: "ADD_TO_CART",
        payload: { id: id, qty: qty, price: price }
      });
    },
    countCartReducer: count => {
      dispatch({
        type: "COUNT_CART",
        payload: { count: count }
      });
    },
    setUser: user => {
      dispatch({
        type: "SET_USER",
        payload: { user: user }
      });
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(SignIn);

const style = StyleSheet.create({
  headerIcon: {
    color: "white"
  },
  textInput: {
    borderRadius: 5,
    width: screenWidth - 20,
    height: 45,
    fontSize: 16,
    marginVertical: 10,
    backgroundColor: "#ddd",
    color: "black"
  },
  register: {
    backgroundColor: "#ff4747",
    marginTop: 10
  },
  registerText: {
    fontSize: 18,
    fontWeight: "500",
    color: "white",
    textAlign: "center"
  },
  spinnerView: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  spinnerBackground: {
    height: screenHeight,
    width: screenWidth,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,0.2)"
  }
});
