import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Text,
  Thumbnail,
  Content
} from "native-base";

import {
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  Dimensions,
  ToastAndroid
} from "react-native";

import { Navigation } from "react-native-navigation";

import { siteUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

import { connect } from "react-redux";

import {
  LoginManager,
  GraphRequest,
  GraphRequestManager
} from "react-native-fbsdk";

import {
  GoogleSignin,
  statusCodes
} from "@react-native-community/google-signin";

const { width: screenWidth } = Dimensions.get("window");

class JoinOrSignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      signInWith: [
        {
          id: 1,
          icon: require("../../assets/icon/google.png")
        },
        {
          id: 2,
          icon: require("../../assets/icon/facebook.png")
        },
        {
          id: 3,
          icon: require("../../assets/icon/twitter.png")
        }
      ]
    };
  }

  componentDidMount() {
    this.configureGoogleSignIn();
  }

  backToScreen = () => {
    Navigation.pop(this.props.componentId);
  };

  registerUser = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "Join",
        passProps: {
          comId: this.props.componentId,
          proID: this.props.proID,
          qty: this.props.qty,
          price: this.props.price,
          status: this.props.status
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };

  signInUser = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "SignIn",
        passProps: {
          comId: this.props.componentId,
          proID: this.props.proID,
          qty: this.props.qty,
          price: this.props.price,
          status: this.props.status
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };

  configureGoogleSignIn() {
    GoogleSignin.configure({
      webClientId:
        "131982394714-ri9vo1fglua796ikigm7t1rlp4jp9lsk.apps.googleusercontent.com",
      offlineAccess: false
    });
  }

  loginGoogle = async () => {
    this.props.addToCart;
    try {
      const userInfo = await GoogleSignin.signIn();

      const params = {
        g_id: userInfo.user.id,
        first_name: userInfo.user.familyName,
        last_name: userInfo.user.givenName,
        email: userInfo.user.email
      };

      ServerClass.responsePostData(
        siteUrl + "mobileApi/registerGoogle",
        params
      ).then(json => {
        if (json.statusCode == "S0001") {
          ServerClass.setUser(json.user_id);
          this.props.setUser(json.user_id);
          this.setState({ user_id: json.user_id });
          if (this.props.status == "wishlist") {
            this.addWishList();
          } else if (this.props.status == "add") {
            this.addItemToCart();
          } else if (this.props.status == "buy") {
            this.buyNow();
          } else {
            Navigation.pop(this.props.componentId);
          }
        }
      });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.warn("user cancelled the login flow");
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.warn("operation (f.e. sign in) is in progress already");
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.warn("play services not available or outdated");
      } else {
        console.warn(error);
      }
    }
  };

  async loginFacebook() {
    try {
      let result = await LoginManager.logInWithPermissions(["public_profile"]);
      if (result.isCancelled) {
        ToastAndroid.show("Login was cancelled ", ToastAndroid.SHORT);
      } else {
        const infoRequest = new GraphRequest(
          "/me?fields=id,first_name,last_name,picture.type(large)",
          null,
          this._responseInfoCallback
        );
        new GraphRequestManager().addRequest(infoRequest).start();
      }
    } catch (error) {
      ToastAndroid.show("An error occured : " + error, ToastAndroid.LONG);
    }
  }

  _responseInfoCallback = (error, result) => {
    if (error) {
      ToastAndroid.show(
        "Error fetching data: " + error.toString(),
        ToastAndroid.SHORT
      );
    } else {
      const params = {
        fb_id: result.id,
        first_name: result.first_name,
        last_name: result.last_name
      };

      ServerClass.responsePostData(
        siteUrl + "mobileApi/registerFB",
        params
      ).then(json => {
        if (json.statusCode == "S0001") {
          ServerClass.setUser(json.user_id);
          this.props.setUser(json.user_id);
          this.setState({ user_id: json.user_id });
          if (this.props.status == "wishlist") {
            this.addWishList();
          } else if (this.props.status == "add") {
            this.addItemToCart();
          } else if (this.props.status == "buy") {
            this.buyNow();
          } else {
            Navigation.pop(this.props.componentId);
          }
        }
      });
    }
  };

  addItemToCart = () => {
    const params = {
      id: this.props.proID,
      price: this.props.price,
      qty: this.props.qty,
      user_id: this.state.user_id
    };
    ServerClass.responsePostData(siteUrl + "mobileApi/addToCart", params).then(
      json => {
        if (json.statusCode == "S0001") {
          this.props.addToCart(
            this.props.proID,
            this.props.qty,
            this.props.price
          );
          this.props.countCartReducer(json.count);
          ToastAndroid.show(
            "Item has been added successful ! ",
            ToastAndroid.SHORT
          );

          Navigation.pop(this.props.componentId);
        }
      }
    );
  };

  addWishList = () => {
    const params = {
      id: this.props.proID,
      user: this.state.user_id
    };
    ServerClass.responsePostData(
      siteUrl + "mobileApi/addWishList",
      params
    ).then(json => {
      if (json.statusCode == "E0002") {
        ToastAndroid.show(
          "This item is already in your wishlist !",
          ToastAndroid.SHORT
        );
      } else {
        ToastAndroid.show("Added !", ToastAndroid.SHORT);

        Navigation.pop(this.props.componentId);
      }
    });
  };

  buyNow = () => {
    const params = {
      id: this.props.proID,
      price: this.props.price,
      qty: this.props.qty,
      user_id: this.state.user_id
    };
    ServerClass.responsePostData(siteUrl + "mobileApi/addToCart", params).then(
      json => {
        if (json.statusCode == "S0001") {
          this.props.addToCart(
            this.props.proID,
            this.props.qty,
            this.props.price
          );
          this.props.countCartReducer(json.count);
          ToastAndroid.show(
            "Item has been added successful ! ",
            ToastAndroid.SHORT
          );
          Navigation.push(this.props.componentId, {
            component: {
              name: "ViewCart",
              passProps: {
                user: this.state.user_id
              },
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                bottomTabs: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                animations: {
                  setStackRoot: {
                    enabled: true
                  }
                }
              }
            }
          });
        }
      }
    );
  };

  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header noShadow style={{ backgroundColor: "transparent" }}>
          <Left>
            <TouchableOpacity onPress={() => this.backToScreen()}>
              <Icon style={style.headerIcon} name="close" />
            </TouchableOpacity>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content>
          <View
            style={{
              alignItems: "center",
              height: 450
            }}
          >
            <Image
              source={require("../../assets/icon/logo.png")}
              style={{ width: 150, height: 84 }}
            />
          </View>

          <View style={{ padding: 10 }}>
            <Button
              block
              style={style.register}
              onPress={() => this.registerUser()}
            >
              <Text style={style.registerText}>REGISTER</Text>
            </Button>
            <Button
              block
              style={style.signin}
              onPress={() => this.signInUser()}
            >
              <Text style={style.signinText}>SIGN IN</Text>
            </Button>
          </View>
          <View style={{ padding: 10 }}>
            <Text style={{ textAlign: "center" }}>Or Sign in with</Text>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
            >
              <TouchableOpacity onPress={() => this.loginGoogle()}>
                <Thumbnail
                  square
                  large
                  source={require("../../assets/icon/google.png")}
                />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => this.loginFacebook()}>
                <Thumbnail
                  square
                  large
                  source={require("../../assets/icon/facebook.png")}
                />
              </TouchableOpacity>
            </ScrollView>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addToCart: (id, qty, price) => {
      dispatch({
        type: "ADD_TO_CART",
        payload: { id: id, qty: qty, price: price }
      });
    },
    countCartReducer: count => {
      dispatch({
        type: "COUNT_CART",
        payload: { count: count }
      });
    },
    setUser: user => {
      dispatch({
        type: "SET_USER",
        payload: { user: user }
      });
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(JoinOrSignIn);

const style = StyleSheet.create({
  top: {
    width: screenWidth,
    position: "absolute"
  },
  headerIcon: {
    color: "black"
  },
  register: {
    backgroundColor: "#ff4747",
    marginTop: 10
  },
  signin: {
    backgroundColor: "#fff1f1",
    marginTop: 10
  },
  registerText: {
    fontSize: 18,
    fontWeight: "500",
    color: "white",
    textAlign: "center"
  },
  signinText: {
    fontSize: 18,
    fontWeight: "500",
    color: "#ff4747",
    textAlign: "center"
  }
});
