import React, { Component } from "react";
import { siteUrl, adminUrl } from "../../class/Constant";
import ServerClass from "../../class/ServerClass";
import {
  Container,
  Header,
  Content,
  Left,
  Icon,
  Item,
  Input,
  Card,
  CardItem,
  Tab,
  Tabs,
  ScrollableTab,
  Button,
  Title,
  Grid,
  Col,
  Right,
  Thumbnail
} from "native-base";

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  ScrollView,
  ActivityIndicator
} from "react-native";

import { Navigation } from "react-native-navigation";

import { connect } from "react-redux";

class Category extends Component {
  mounted = false;
  static options() {
    return {
      topBar: {
        drawBehind: true,
        visible: false,
        animate: false
      }
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      cateItem: [],
      hotDeal: [],
      lastID: 0,
      isRefresh: false,
      count: 0,
      isLoading: false,
      user: 0
    };
  }

  responseCategory = () => {
    ServerClass.responsGetData(siteUrl + "mobileApi/responseCategory").then(
      json => {
        var checkArr = [];
        let cateLength = json.length;
        for (i = 0; i < cateLength; i++) {
          checkArr.push({
            product_category_id: json[i].product_category_id,
            product_category_name: json[i].product_category_name,
            product_category_icon: json[i].product_category_icon,
            checked: false,
            selected: "#D0D3D4"
          });
        }
        this.setState({ cateItem: checkArr });
      }
    );
  };

  componentDidMount() {
    this.mounted = true;
    ServerClass.checkUser().then(val => {
      this.setState({ user: val });
      this.responseCategory();
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  componentDidUpdate(prevProps) {
    if (this.props.userID.user_id !== prevProps.userID.user_id) {
      ServerClass.checkUser().then(val => {
        this.setState({ user: val });
      });
    }
  }

  goToScreen = text => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "Search",
        passProps: {
          type: text
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };
  renderLoading = () => {
    if (!this.state.isLoading) return null;
    return <ActivityIndicator size="large" color="red" />;
  };
  goToProduct = id => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "Product",
        passProps: {
          data: id,
          user: this.state.user
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };

  handleLoadMore = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };

  searchModal = () => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "Search",
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                bottomTabs: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                animations: {
                  setStackRoot: {
                    enabled: true
                  }
                }
              }
            }
          }
        ]
      }
    });
  };

  handlingSelectedCategory = index => {
    // this.setState({ selectedCate: "#ff4747" });
    const cateItem = [...this.state.cateItem];
    const newArray = cateItem.map(item => {
      return { ...item, checked: false, selected: "#D0D3D4" };
    });
    newArray[index].selected = "#ff4747";
    this.setState({
      cateItem: newArray
    });
  };

  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header noShadow style={{ backgroundColor: "#1f2024" }}>
          <Left>
            <Title style={style.headerTitle}>Categories</Title>
          </Left>

          <Right>
            <Button vertical transparent>
              <Icon name="search"></Icon>
            </Button>
          </Right>
        </Header>
        <Grid>
          <Col size={1}>
            <ScrollView horizontal={false} showsVerticalScrollIndicator={false}>
              {/* <TouchableOpacity
                key={item.product_category_id}
                style={{
                  padding: 10,
                  borderRadius: 8,
                  borderStyle: "solid",
                  borderColor: item.selected,
                  borderRightWidth: 2,
                  borderBottomWidth: 2
                }}
                onPress={() => this.handlingSelectedCategory(index)}
              >
                <Image
                  source={{
                    uri:
                      adminUrl +
                      "images/category_icon/" +
                      item.product_category_icon
                  }}
                  style={style.categoryIcon}
                />

                <Text style={style.categoryText}>
                  {item.product_category_name}
                </Text>
              </TouchableOpacity> */}
              {this.state.cateItem.map((item, index) => {
                return (
                  <TouchableOpacity
                    key={item.product_category_id}
                    style={{
                      padding: 10,
                      borderRadius: 8,
                      borderStyle: "solid",
                      borderColor: item.selected,
                      borderRightWidth: 1,
                      borderBottomWidth: 1
                    }}
                    onPress={() => this.handlingSelectedCategory(index)}
                  >
                    <Image
                      source={{
                        uri:
                          adminUrl +
                          "images/category_icon/" +
                          item.product_category_icon
                      }}
                      style={style.categoryIcon}
                    />

                    <Text style={style.categoryText}>
                      {item.product_category_name}
                    </Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </Col>
          <Col size={2}></Col>
        </Grid>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    userID: state
  };
};

export default connect(mapStateToProps)(Category);

const style = StyleSheet.create({
  header: {
    backgroundColor: "#ff4747",
    color: "white"
  },
  headerTitle: {
    color: "white",
    alignSelf: "center",
    fontSize: 18
  },
  footerTab: {
    backgroundColor: "white"
  },
  footerThumbnail: {
    width: 40,
    height: 27
  },
  footerText: {
    color: "black"
  },

  categoryIcon: {
    width: 50,
    height: 50,
    alignSelf: "center"
  },
  categoryText: {
    textAlign: "center",
    fontSize: 12
  },
  emptyProduct: {
    alignItems: "center",
    alignSelf: "center",
    marginTop: -140
  },
  emptyImage: {
    width: 150,
    height: 150
  },
  customCard: {
    width: "100%",
    borderRadius: 10
  },
  itemImage: {
    width: "100%",
    height: "100%",
    aspectRatio: 1,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8
  },
  itemPrice: {
    fontWeight: "bold",
    fontSize: 12,
    color: "black"
  },
  itemText: {
    fontWeight: "bold",
    fontSize: 10,
    color: "black"
  }
});
