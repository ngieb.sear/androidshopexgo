import React, { Component } from "react";

import {
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
  Title,
  Text,
  Card,
  CardItem,
  Thumbnail,
  Button,
  Footer,
  FooterTab,
  Icon,
  Input,
  Item
} from "native-base";

import { Grid, Col } from "react-native-easy-grid";

import {
  StyleSheet,
  Image,
  CheckBox,
  TouchableOpacity,
  View,
  FlatList,
  ActivityIndicator,
  Dimensions,
  ToastAndroid,
  Alert
} from "react-native";

import NumericInput from "react-native-numeric-input";

import { Navigation } from "react-native-navigation";

import { siteUrl, sellerUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

import { connect } from "react-redux";

const { width: screenWidth } = Dimensions.get("window");

class Cart extends Component {
  mounted = false;
  static options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
        animate: false
      }
    };
  }
  constructor(props) {
    super(props);

    this.state = {
      cart: [],
      cartDetail: [],
      user: 0,
      lastID: 0,
      qty: 0,
      moreToLove: [],
      checkAll: false,
      checkItem: [],
      totalAmount: 0.0,
      countItem: 0
    };
  }

  componentDidMount = () => {
    this.mounted = true;
    ServerClass.checkUser().then(val => {
      this.setState({ user: val });
      this.responseCartItem();
    });
  };

  componentWillUnmount() {
    this.mounted = false;
  }

  componentDidUpdate(prevProps) {
    if (this.props.countItem.user_id !== prevProps.countItem.user_id) {
      ServerClass.checkUser().then(val => {
        this.setState({ user: val });
        this.responseCartItem();
      });
    }
    if (
      this.props.countItem.countCartReducer !==
      prevProps.countItem.countCartReducer
    ) {
      this.responseCartItem();
    }
  }

  responseCartItem = () => {
    if (this.state.user > 0) {
      const params = {
        user_id: this.state.user
      };
      if (this.mounted) {
        ServerClass.responsePostData(
          siteUrl + "mobileApi/responseCartItem",
          params
        ).then(json => {
          var cart = [];
          var cartDetail = [];
          var checkItem = [];

          for (i = 0; i < json.Cart.length; i++) {
            if (json.Cart[i].cart_id != undefined) {
              cart.push({
                cart_id: json.Cart[i].cart_id,
                cart_amount: json.Cart[i].cart_amount,
                cart_status: json.Cart[i].cart_status
              });

              for (j = 0; j < json.Cart[i].Detail.length; j++) {
                cartDetail.push({
                  cart_detail_id: json.Cart[i].Detail[j].cart_detail_id,
                  product_name: json.Cart[i].Detail[j].product_name,
                  product_photo: json.Cart[i].Detail[j].product_photo,
                  product_detail_id: json.Cart[i].Detail[j].product_detail_id,
                  qty: json.Cart[i].Detail[j].qty,
                  price: json.Cart[i].Detail[j].price,
                  sub_total: json.Cart[i].Detail[j].sub_total,
                  checked: false
                });
              }
            }
          }

          this.setState({
            cart: cart,
            cartDetail: cartDetail,
            checkItem: checkItem,
            countItem: cartDetail.length
          });
        });
      }
    }
    this.responseProduct();
  };

  responseProduct = () => {
    const params = {
      offset: this.state.lastID
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseProduct",
      params
    ).then(json => {
      this.setState({
        moreToLove: this.state.moreToLove.concat(json)
      });
    });
  };

  handleLoadMore = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };

  goToScreen = (id, screen) => {
    if (screen == 1) {
      Navigation.push(this.props.componentId, {
        component: {
          name: "Product",
          passProps: {
            data: id,
            user: this.state.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    } else if (screen == 2) {
      let cartLength = this.state.cartDetail.length;
      const cartData = [...this.state.cartDetail];
      var checkArr = [];
      var totalAmount = 0;
      for (i = 0; i < cartLength; i++) {
        if (cartData[i].checked) {
          totalAmount += parseFloat(cartData[i].sub_total);
          checkArr.push({
            cart_detail_id: cartData[i].cart_detail_id,
            product_name: cartData[i].product_name,
            product_photo: cartData[i].product_photo,
            product_detail_id: cartData[i].product_detail_id,
            qty: cartData[i].qty,
            price: cartData[i].price,
            sub_total: cartData[i].sub_total,
            checked: true
          });
        }
      }

      this.setState({ totalAmount: totalAmount });

      if (checkArr.length > 0) {
        Alert.alert(
          "Continue",
          "Are you sure you want to continue the items selected ?",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            {
              text: "Continue",
              onPress: () => {
                const params = {
                  user_id: this.state.user
                };

                ServerClass.responsePostData(
                  siteUrl + "mobileApi/checkShippingDefault",
                  params
                ).then(json => {
                  var screenName = "OrderConfirm";
                  if (json.statusCode == "S0001") {
                    screenName = screenName;
                  } else {
                    screenName = "DeliveryInformation";
                  }

                  Navigation.push(this.props.componentId, {
                    component: {
                      name: screenName,
                      passProps: {
                        user: this.state.user,
                        checkArr: checkArr,
                        totalAmount: totalAmount
                      },
                      options: {
                        topBar: {
                          visible: false,
                          drawBehind: true,
                          animate: false
                        },
                        bottomTabs: {
                          visible: false,
                          drawBehind: true,
                          animate: false
                        },
                        animations: {
                          setStackRoot: {
                            enabled: true
                          }
                        }
                      }
                    }
                  });
                });
              }
            }
          ],
          { cancelable: false }
        );
      } else {
        ToastAndroid.show(
          "Please select which item you want to buy ",
          ToastAndroid.SHORT
        );
      }
    } else {
      Navigation.pop(this.props.componentId);
    }
  };

  handleCheckAll() {
    let arr_length = this.state.cartDetail.length;
    const newArray = [...this.state.cartDetail];
    var total = 0;
    for (i = 0; i < arr_length; i++) {
      newArray[i].checked = !this.state.checkAll;
      total += parseFloat(newArray[i].sub_total);
    }
    if (!this.state.checkAll) {
      total = total;
    } else {
      total = 0.0;
    }
    this.setState({
      cartDetail: newArray,
      checkAll: !this.state.checkAll,
      totalAmount: total.toFixed(2)
    });
  }

  handleCheckItem(id, index) {
    var total = 0;
    const newArray = [...this.state.cartDetail];
    newArray[index].checked = !newArray[index].checked;

    if (newArray[index].checked) {
      total =
        parseFloat(this.state.totalAmount) +
        parseFloat(newArray[index].sub_total);
    } else {
      total =
        parseFloat(this.state.totalAmount) -
        parseFloat(newArray[index].sub_total);
    }

    this.setState({
      cartDetail: newArray,
      checkAll: false,
      totalAmount: total.toFixed(2)
    });
  }

  removeItemFromCart = () => {
    let cartLength = this.state.cartDetail.length;
    const cartData = [...this.state.cartDetail];
    var deleteArr = [];
    for (i = 0; i < cartLength; i++) {
      if (cartData[i].checked) {
        deleteArr.push(cartData[i].cart_detail_id);
      }
    }
    if (deleteArr.length > 0) {
      Alert.alert(
        "Remove",
        "Are you sure you want to remove the items selected ?",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          {
            text: "Remove",
            onPress: () => {
              const params = {
                detail_id: deleteArr
              };

              ServerClass.responsePostData(
                siteUrl + "mobileApi/removeItemFromCart",
                params
              ).then(json => {
                if (json.statusCode == "S0001") {
                  this.responseCartItem();
                  ToastAndroid.show(
                    cartLength + " item(s) has been removed ",
                    ToastAndroid.SHORT
                  );
                  this.setState({
                    totalAmount: 0.0
                  });
                }
              });
            }
          }
        ],
        { cancelable: false }
      );
    } else {
      ToastAndroid.show(
        "Please select which item you want to remove ",
        ToastAndroid.SHORT
      );
    }
  };

  updateQty = (detail_id, qty, price) => {
    this.setState({ qty: qty });
    const params = {
      detail_id: detail_id,
      qty: qty,
      price: price
    };

    ServerClass.responsePostData(siteUrl + "mobileApi/updateQty", params).then(
      json => {
        if (json.statusCode == "S0001") {
          this.responseCartItem();
        }
      }
    );
  };

  render() {
    return (
      <Container style={style.container}>
        <Header noShadow style={{ backgroundColor: "transparent" }}>
          <Left>
            <Title style={style.headerTitle}>
              Cart ({this.state.countItem})
            </Title>
          </Left>

          <Right>
            <Button
              vertical
              transparent
              onPress={() => this.removeItemFromCart()}
            >
              <Thumbnail
                square
                small
                source={require("../../assets/icon/delete.png")}
              />
            </Button>
          </Right>
        </Header>
        <Content
          onScroll={({ nativeEvent }) => {
            if (this.handleLoadMore(nativeEvent)) {
              this.setState(
                { lastID: this.state.lastID + 20 },
                this.responseProduct
              );
            }
          }}
        >
          {this.state.cartDetail.length > 0 ? (
            this.state.cartDetail.map((item, index) => {
              return (
                <Card transparent key={item.cart_detail_id}>
                  <CardItem>
                    <Left>
                      <CheckBox
                        value={item.checked}
                        onChange={() =>
                          this.handleCheckItem(item.product_detail_id, index)
                        }
                      ></CheckBox>
                      <Image
                        source={{
                          uri:
                            sellerUrl + "images/product/" + item.product_photo
                        }}
                        style={style.itemImage}
                      />
                    </Left>
                    <Body>
                      <Text>
                        {item.product_name.slice(0, 50) +
                          (item.product_name.length > 50 ? "..." : "")}
                      </Text>

                      <Text style={style.itemPrice}>US ${item.price}</Text>

                      <NumericInput
                        value={Number(item.qty)}
                        onChange={qty =>
                          this.updateQty(item.cart_detail_id, qty, item.price)
                        }
                        totalWidth={150}
                        totalHeight={35}
                        iconSize={15}
                        step={1}
                        valueType="real"
                        rounded
                        textColor="#ff4747"
                        iconStyle={{ color: "black" }}
                        rightButtonBackgroundColor="#ddd"
                        leftButtonBackgroundColor="#ddd"
                        minValue={Number(1)}
                      />
                    </Body>
                  </CardItem>
                </Card>
              );
            })
          ) : (
            <Card transparent>
              <CardItem cardBody>
                <Image
                  source={require("../../assets/icon/empty_cart.jpg")}
                  style={{
                    width: "100%",
                    height: "100%",
                    aspectRatio: 1,
                    resizeMode: "cover",
                    alignSelf: "center"
                  }}
                />
              </CardItem>
            </Card>
          )}

          <View style={{ paddingLeft: 13, paddingRight: 13, marginTop: 10 }}>
            <Text style={{ fontWeight: "500", fontSize: 18, color: "black" }}>
              More To Love
            </Text>
            <FlatList
              data={this.state.moreToLove}
              contentContainerStyle={{ flexGrow: 1 }}
              renderItem={({ item }) => (
                <View
                  style={{
                    flex: 1 / 2
                  }}
                >
                  <Card style={{ borderRadius: 10 }}>
                    <CardItem
                      cardBody
                      style={{ alignSelf: "center", borderRadius: 10 }}
                      button
                      onPress={() => this.goToScreen(item.product_id, 1)}
                    >
                      <Image
                        source={{
                          uri:
                            sellerUrl + "images/product/" + item.product_photo
                        }}
                        style={style.itemImage}
                      />
                    </CardItem>

                    <CardItem
                      style={{ height: 50 }}
                      button
                      onPress={() => this.goToScreen(item.product_id, 1)}
                    >
                      <Body>
                        <Text style={style.itemText}>
                          {item.product_name.slice(0, 35) +
                            (item.product_name.length > 35 ? "..." : "")}
                        </Text>
                      </Body>
                    </CardItem>

                    <CardItem
                      footer
                      style={{
                        borderBottomLeftRadius: 10,
                        borderBottomRightRadius: 10
                      }}
                      button
                      onPress={() => this.goToScreen(item.product_id, 1)}
                    >
                      <Left>
                        <Text style={style.itemPrice}>
                          US ${item.promotion_price}
                        </Text>
                      </Left>
                    </CardItem>
                  </Card>
                </View>
              )}
              keyExtractor={(item, index) => item.product_id}
              numColumns={2}
            />
          </View>
          <ActivityIndicator size="large" color="red" />
        </Content>
        {this.state.cartDetail.length > 0 ? (
          <Footer style={{ backgroundColor: "white" }}>
            <FooterTab style={{ backgroundColor: "white" }}>
              <Grid>
                <Col style={{ alignItems: "flex-start" }}>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                      padding: 10
                    }}
                  >
                    <CheckBox
                      value={this.state.checkAll}
                      onChange={() => this.handleCheckAll()}
                    />
                    <Text style={{ alignSelf: "center", textAlign: "center" }}>
                      All
                    </Text>
                  </View>
                </Col>
                <Col style={{ alignItems: "flex-end" }}>
                  <View
                    style={{
                      padding: 10,
                      alignItems: "center",
                      justifyContent: "center",
                      width: "100%",
                      height: "100%"
                    }}
                  >
                    <Title style={style.totalAmount}>
                      US ${this.state.totalAmount}
                    </Title>
                  </View>
                </Col>
                <Col style={{ alignItems: "flex-end" }}>
                  <View
                    style={{
                      padding: 10,
                      alignItems: "center",
                      justifyContent: "center",
                      width: "100%",
                      height: "100%"
                    }}
                  >
                    <Button
                      style={style.buy}
                      onPress={() => this.goToScreen(0, 2)}
                    >
                      <Text style={style.buyText}>Buy</Text>
                    </Button>
                  </View>
                </Col>
              </Grid>
            </FooterTab>
          </Footer>
        ) : null}
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    countItem: state
  };
};

export default connect(mapStateToProps)(Cart);

const style = StyleSheet.create({
  container: {
    backgroundColor: "#f7f7f7"
  },
  top: {
    width: screenWidth,
    backgroundColor: "white"
  },
  header: {
    marginTop: 40,
    backgroundColor: "white"
  },

  headerTitle: {
    fontSize: 20,
    fontWeight: "500",
    color: "black"
  },

  itemImage: {
    width: "100%",
    height: "100%",
    aspectRatio: 1,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8
  },
  itemPrice: {
    fontWeight: "bold",
    fontSize: 12,
    color: "black"
  },
  itemText: {
    fontWeight: "bold",
    fontSize: 10,
    color: "black"
  },
  totalAmount: {
    fontWeight: "500",
    fontSize: 18,
    color: "black"
  },
  buy: {
    backgroundColor: "#ff4747",
    width: "100%",
    borderRadius: 5
  },
  buyText: {
    fontSize: 15,
    fontWeight: "500",
    color: "white"
  }
});
