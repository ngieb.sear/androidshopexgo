import React, { Component } from "react";
import { siteUrl, baseUrl } from "../../class/Constant";
import ServerClass from "../../class/ServerClass";
import {
  Container,
  Header,
  Content,
  Left,
  Icon,
  Item,
  Input,
  Card,
  CardItem,
  Tab,
  Tabs,
  ScrollableTab,
  Button
} from "native-base";

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  ActivityIndicator
} from "react-native";
import { Navigation } from "react-native-navigation";

import { connect } from "react-redux";

class Deal extends Component {
  mounted = false;
  static options() {
    return {
      topBar: {
        drawBehind: true,
        visible: false,
        animate: false
      }
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      cateItem: [],
      hotDeal: [],
      lastID: 0,
      isRefresh: false,
      count: 0,
      isLoading: false,
      user: 0
    };
  }

  responseCategory = () => {
    ServerClass.responsGetData(siteUrl + "mobileApi/responseCategory").then(
      json => {
        this.setState({ cateItem: json });
      }
    );
  };

  responseDealProduct = () => {
    this.setState({ isRefresh: true });
    if (this.mounted) {
      this.setState({ isLoading: true });
      const params = {
        offset: this.state.lastID
      };
      ServerClass.responsePostData(
        siteUrl + "mobileApi/responseAlldeal",
        params
      ).then(json => {
        this.setState({
          hotDeal: this.state.hotDeal.concat(json),
          isRefresh: false,
          isLoading: false
        });
      });
    }
  };
  componentDidMount() {
    this.mounted = true;
    ServerClass.checkUser().then(val => {
      this.setState({ user: val });
      this.responseCategory();
      this.responseDealProduct();
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  componentDidUpdate(prevProps) {
    if (this.props.userID.user_id !== prevProps.userID.user_id) {
      ServerClass.checkUser().then(val => {
        this.setState({ user: val });
      });
    }
  }

  goToScreen = text => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "Search",
        passProps: {
          type: text
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };
  renderLoading = () => {
    if (!this.state.isLoading) return null;
    return <ActivityIndicator size="large" color="red" />;
  };
  goToProduct = id => {
    Navigation.push(this.props.componentId, {
      component: {
        name: "Product",
        passProps: {
          data: id,
          user: this.state.user
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };

  handleLoadMore = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };

  searchModal = () => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "Search",
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                bottomTabs: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                },
                animations: {
                  setStackRoot: {
                    enabled: true
                  }
                }
              }
            }
          }
        ]
      }
    });
  };

  render() {
    return (
      <Container style={{ backgroundColor: "#f2f2f2" }}>
        <Header
          searchBar
          rounded
          noShadow
          style={{ backgroundColor: "#ff4747" }}
        >
          <Item>
            <Icon name="ios-search" />
            <Input
              placeholder="I'm looking for..."
              onTouchStart={() => this.searchModal()}
            />
          </Item>
          <Button transparent>
            <Text>Search</Text>
          </Button>
        </Header>
        <Tabs renderTabBar={() => <ScrollableTab />}>
          <Tab
            heading="Picked for you"
            tabStyle={{ backgroundColor: "#ff4747" }}
            activeTabStyle={{ color: "#fff", backgroundColor: "#ff4747" }}
          >
            <Content
              onScroll={({ nativeEvent }) => {
                if (this.handleLoadMore(nativeEvent)) {
                  this.setState(
                    { lastID: this.state.lastID + 20 },
                    this.responseDealProduct
                  );
                }
              }}
              style={{
                paddingLeft: 13,
                paddingRight: 13,
                backgroundColor: "#f2f2f2"
              }}
            >
              {this.state.hotDeal.length > 0 ? (
                <FlatList
                  data={this.state.hotDeal}
                  contentContainerStyle={{ flexGrow: 1 }}
                  renderItem={({ item }) => (
                    <View
                      style={{
                        flex: 1 / 2
                      }}
                    >
                      <Card style={{ borderRadius: 10 }}>
                        <CardItem
                          cardBody
                          style={{ alignSelf: "center", borderRadius: 10 }}
                          button
                          onPress={() => this.goToProduct(item.product_id)}
                        >
                          <Image
                            source={{
                              uri:
                                sellerUrl +
                                "images/product/" +
                                item.product_photo
                            }}
                            style={style.itemImage}
                          />
                        </CardItem>

                        <CardItem
                          style={{ height: 50 }}
                          button
                          onPress={() => this.goToProduct(item.product_id)}
                        >
                          <Body>
                            <Text style={style.itemText}>
                              {item.product_name.slice(0, 35) +
                                (item.product_name.length > 35 ? "..." : "")}
                            </Text>
                          </Body>
                        </CardItem>

                        <CardItem
                          footer
                          style={{
                            borderBottomLeftRadius: 10,
                            borderBottomRightRadius: 10
                          }}
                          button
                          onPress={() => this.goToProduct(item.product_id)}
                        >
                          <Left>
                            <Text style={style.itemPrice}>
                              US ${item.promotion_price}
                            </Text>
                          </Left>
                        </CardItem>

                        <CardItem>
                          <Left>
                            <Text
                              style={{
                                textDecorationLine: "line-through",
                                textDecorationStyle: "solid"
                              }}
                            >
                              US ${item.product_price}
                            </Text>
                            <Text> - </Text>
                            <Text>{item.promotion_discount}% off</Text>
                          </Left>
                        </CardItem>
                      </Card>
                    </View>
                  )}
                  keyExtractor={(item, index) => item.product_id}
                  numColumns={2}
                />
              ) : (
                <View
                  style={{
                    backgroundColor: "#f2f2f2"
                  }}
                >
                  <Image
                    source={require("../../assets/icon/placeholder.png")}
                    style={{
                      width: "100%",
                      height: "100%",
                      aspectRatio: 1,
                      resizeMode: "cover",
                      alignSelf: "center"
                    }}
                  />
                </View>
              )}
            </Content>
          </Tab>
          {this.state.cateItem.map((item, i) => {
            return (
              <Tab
                key={item.product_category_id}
                heading={item.product_category_name}
                tabStyle={{ backgroundColor: "#ff4747" }}
                activeTabStyle={{ color: "#fff", backgroundColor: "#ff4747" }}
              >
                <DealProduct
                  id={item.product_category_id}
                  comID={this.props.componentId}
                  user={this.state.user}
                />
              </Tab>
            );
          })}
        </Tabs>
      </Container>
    );
  }
}

class DealProduct extends Component {
  constructor(props) {
    super();
    this.state = {
      dealData: [],
      lastID: 0,
      isLoading: false
    };
  }

  componentWillMount() {
    this.responseHotDealProductByCat();
  }
  componentWillUnmount() {}

  responseHotDealProductByCat = () => {
    this.setState({ isRefresh: true, isLoading: true });

    const params = {
      offset: this.state.lastID,
      id: this.props.id
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseHotDealProductByCat",
      params
    ).then(json => {
      this.setState({
        dealData: this.state.dealData.concat(json),
        isLoading: false
      });
    });
  };

  renderLoading = () => {
    if (!this.state.isLoading) return null;
    return <ActivityIndicator size="large" color="red" />;
  };
  goToProduct = id => {
    Navigation.push(this.props.comID, {
      component: {
        name: "Product",
        passProps: {
          data: id,
          user: this.props.user
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };
  handleLoadMore = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };
  render() {
    return (
      <Content
        onScroll={({ nativeEvent }) => {
          if (this.handleLoadMore(nativeEvent)) {
            this.setState(
              { lastID: this.state.lastID + 20 },
              this.responseDealProductByCat
            );
          }
        }}
        style={{
          paddingLeft: 13,
          paddingRight: 13,
          backgroundColor: "#f2f2f2"
        }}
      >
        {this.state.dealData.length > 0 ? (
          <FlatList
            data={this.state.dealData}
            renderItem={({ item }) => (
              <View style={{ flex: 1, flexDirection: "column" }}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => this.goToProduct(item.product_id)}
                >
                  <Card padder style={{ borderRadius: 10 }}>
                    <CardItem
                      cardBody
                      style={{ alignSelf: "center", borderRadius: 10 }}
                    >
                      <Image
                        source={{
                          uri: baseUrl + "images/product/" + item.product_photo
                        }}
                        style={style.itemImage}
                      />
                    </CardItem>

                    <CardItem>
                      <Left>
                        <Text>
                          {item.product_name.slice(0, 20) +
                            (item.product_name.length > 20 ? "..." : "")}
                        </Text>
                      </Left>
                    </CardItem>
                    <CardItem
                      footer
                      style={{
                        borderBottomLeftRadius: 10,
                        borderBottomRightRadius: 10
                      }}
                    >
                      <Left>
                        <Text style={style.itemPrice}>
                          US ${item.promotion_price}
                        </Text>
                      </Left>
                    </CardItem>
                    <CardItem>
                      <Left>
                        <Text
                          style={{
                            textDecorationLine: "line-through",
                            textDecorationStyle: "solid"
                          }}
                        >
                          US ${item.product_price}
                        </Text>
                        <Text> - </Text>
                        <Text>{item.promotion_discount}% off</Text>
                      </Left>
                    </CardItem>
                  </Card>
                </TouchableOpacity>
              </View>
            )}
            keyExtractor={item => item.product_id}
            numColumns={2}
            ListFooterComponent={this.renderLoading.bind(this)}
          />
        ) : (
          <View
            style={{
              backgroundColor: "#f2f2f2"
            }}
          >
            <Image
              source={require("../../assets/icon/placeholder.png")}
              style={{
                width: "100%",
                height: "100%",
                aspectRatio: 1,
                resizeMode: "cover",
                alignSelf: "center"
              }}
            />
          </View>
        )}
      </Content>
    );
  }
}

const mapStateToProps = state => {
  return {
    userID: state
  };
};

export default connect(mapStateToProps)(Deal);

const style = StyleSheet.create({
  header: {
    backgroundColor: "#ff4747",
    color: "white"
  },
  headerTitle: {
    color: "white",
    alignSelf: "center",
    fontSize: 22,
    marginRight: 20
  },
  footerTab: {
    backgroundColor: "white"
  },
  footerThumbnail: {
    width: 40,
    height: 27
  },
  footerText: {
    color: "black"
  },
  btnCategory: {
    width: 100,
    height: null
  },
  categoryIcon: {
    width: 50,
    height: 50,
    alignSelf: "center"
  },
  categoryText: {
    textAlign: "center"
  },
  emptyProduct: {
    alignItems: "center",
    alignSelf: "center",
    marginTop: -140
  },
  emptyImage: {
    width: 150,
    height: 150
  },
  customCard: {
    width: "100%",
    borderRadius: 10
  },
  itemImage: {
    width: "100%",
    height: "100%",
    aspectRatio: 1,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8
  },
  itemPrice: {
    fontWeight: "bold",
    fontSize: 12,
    color: "black"
  },
  itemText: {
    fontWeight: "bold",
    fontSize: 10,
    color: "black"
  }
});
