import React, { Component } from "react";

import {
  Container,
  Header,
  Content,
  Left,
  Body,
  Right,
  Title,
  Text,
  Card,
  CardItem,
  Button,
  Icon
} from "native-base";

import {
  StyleSheet,
  Image,
  TouchableOpacity,
  View,
  FlatList,
  ActivityIndicator
} from "react-native";

import { Navigation } from "react-native-navigation";

import { siteUrl, baseUrl } from "../../class/Constant";

import ServerClass from "../../class/ServerClass";

class Reviewed extends Component {
  mounted = false;
  constructor(props) {
    super(props);

    this.state = {
      reviewedData: [],
      user: 0,
      lastID: 0,
      qty: 0,
      moreToLove: [],
      checkAll: false,
      checkItem: [],
      totalAmount: 0.0,
      countItem: 0
    };
  }

  componentDidMount = () => {
    this.mounted = true;
    this.responseReviewed();
  };

  componentWillUnmount() {
    this.mounted = false;
  }

  responseReviewed = () => {
    if (this.props.user > 0) {
      const params = {
        user_id: this.props.user
      };

      if (this.mounted) {
        ServerClass.responsePostData(
          siteUrl + "mobileApi/responseReviewed",
          params
        ).then(json => {
          this.setState({ reviewedData: json });
        });
      }
    }
    this.responseProduct();
  };

  responseProduct = () => {
    const params = {
      offset: this.state.lastID
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseProduct",
      params
    ).then(json => {
      this.setState({
        moreToLove: this.state.moreToLove.concat(json)
      });
    });
  };

  handleLoadMore = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };

  goToScreen = (id, screen) => {
    Navigation.push(this.props.componentId, {
      component: {
        name: screen,
        passProps: {
          data: id,
          user: this.props.user
        },
        options: {
          topBar: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          bottomTabs: {
            visible: false,
            drawBehind: true,
            animate: false
          },
          animations: {
            setStackRoot: {
              enabled: true
            }
          }
        }
      }
    });
  };

  render() {
    return (
      <Container style={style.container}>
        <Header noShadow style={{ backgroundColor: "black" }}>
          <Left>
            <Button
              transparent
              onPress={() => Navigation.pop(this.props.componentId)}
            >
              <Icon name="arrow-back" />
            </Button>
          </Left>

          <Body>
            <Title style={style.headerTitle}>To be reviewed</Title>
          </Body>

          <Right></Right>
        </Header>

        <Content
          onScroll={({ nativeEvent }) => {
            if (this.handleLoadMore(nativeEvent)) {
              this.setState(
                { lastID: this.state.lastID + 20 },
                this.responseProduct
              );
            }
          }}
        >
          {this.state.reviewedData.length > 0 ? (
            this.state.reviewedData.map((item, index) => {
              return (
                <Card transparent key={item.order_id}>
                  <CardItem>
                    <Body>
                      <View>
                        <Text>Order ID : {item.order_id}</Text>
                      </View>

                      <View>
                        <Text>Order Date :{item.order_date}</Text>
                      </View>
                    </Body>
                  </CardItem>

                  <ReviewedDetail
                    orderID={item.order_id}
                    user={this.props.user}
                  ></ReviewedDetail>
                </Card>
              );
            })
          ) : (
            <Card transparent>
              <CardItem cardBody>
                <Image
                  source={require("../../assets/icon/empty_cart.jpg")}
                  style={{
                    width: "100%",
                    height: "100%",
                    aspectRatio: 1,
                    resizeMode: "cover",
                    alignSelf: "center"
                  }}
                />
              </CardItem>
            </Card>
          )}

          <View style={{ paddingLeft: 13, paddingRight: 13, marginTop: 10 }}>
            <Text style={{ fontWeight: "500", fontSize: 18, color: "black" }}>
              More To Love
            </Text>
            <FlatList
              data={this.state.moreToLove}
              renderItem={({ item }) => (
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => this.goToScreen(item.product_id, "Product")}
                  >
                    <Card padder style={{ borderRadius: 10 }}>
                      <CardItem
                        cardBody
                        style={{ alignSelf: "center", borderRadius: 10 }}
                      >
                        <Image
                          source={{
                            uri:
                              baseUrl + "images/product/" + item.product_photo
                          }}
                          style={{
                            width: "100%",
                            height: undefined,
                            aspectRatio: 1,
                            borderTopLeftRadius: 8,
                            borderTopRightRadius: 8
                          }}
                        />
                      </CardItem>

                      <CardItem>
                        <Left>
                          <Text>
                            {item.product_name.slice(0, 20) +
                              (item.product_name.length > 20 ? "..." : "")}
                          </Text>
                        </Left>
                      </CardItem>

                      <CardItem footer>
                        <Left>
                          <Text style={style.itemPrice}>
                            US ${item.promotion_price}
                          </Text>
                        </Left>
                      </CardItem>
                    </Card>
                  </TouchableOpacity>
                </View>
              )}
              keyExtractor={(item, index) => item.product_id}
              numColumns={2}
            />
          </View>
          <ActivityIndicator size="large" color="red" />
        </Content>
      </Container>
    );
  }
}

class ReviewedDetail extends Component {
  constructor(props) {
    super();

    this.state = {
      DetailData: []
    };
  }

  componentWillMount() {
    this.responseReviewedDetail();
  }

  responseReviewedDetail = () => {
    const params = {
      order_id: this.props.orderID
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseReviewedDetail",
      params
    ).then(json => {
      this.setState({ DetailData: json });
    });
  };

  render() {
    return this.state.DetailData.map(item => {
      return (
        <CardItem key={item.order_detail_id}>
          <Left>
            <Image
              source={{
                uri: baseUrl + "images/product/" + item.product_photo
              }}
              style={style.itemImage}
            />
          </Left>
          <Body>
            <View style={style.viewContainer}>
              <Text>
                {item.product_name_en.slice(0, 50) +
                  (item.product_name_en.length > 50 ? "..." : "")}
              </Text>
            </View>

            <View style={style.viewContainer}>
              <Text>
                {item.color_name} + {item.size_name}
              </Text>
            </View>
            <View style={style.viewContainer}>
              <Text style={style.itemPrice}>
                US ${item.price} x {item.qty}
              </Text>
            </View>
          </Body>
        </CardItem>
      );
    });
  }
}

export default Reviewed;

const style = StyleSheet.create({
  container: {
    backgroundColor: "#f7f7f7"
  },

  itemImage: {
    borderColor: "#ddd",
    borderWidth: 1,
    height: 128,
    width: 128,
    aspectRatio: 1,
    resizeMode: "stretch",
    borderRadius: 8,
    padding: 10,
    marginRight: 5
  },
  itemPrice: {
    fontWeight: "500",
    fontSize: 18,
    color: "black"
  },
  totalAmount: {
    fontWeight: "500",
    fontSize: 18,
    color: "black"
  },
  buy: {
    backgroundColor: "#ff4747",
    width: "100%",
    borderRadius: 5
  },
  buyText: {
    fontSize: 15,
    fontWeight: "500",
    color: "white"
  },
  viewContainer: {
    marginTop: 5
  }
});
