import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Icon,
  Left,
  Body,
  Right,
  Title
} from "native-base";

import { WebView } from "react-native-webview";
import { Navigation } from "react-native-navigation";

class PrivacyPolicy extends Component {
  goToScreen = () => {
    Navigation.pop(this.props.componentId);
  };
  render() {
    return (
      <Container>
        <Header noShadow style={{ backgroundColor: "black" }}>
          <Left>
            <Icon
              name="arrow-back"
              style={{ color: "white" }}
              onPress={() => this.goToScreen()}
            />
          </Left>
          <Body>
            <Title>Privacy Policy</Title>
          </Body>

          <Right></Right>
        </Header>
        <Content contentContainerStyle={{ flex: 1 }}>
          <WebView
            source={{ uri: "https://www.m.loyloy7.com/home/privacy_policy" }}
          />
        </Content>
      </Container>
    );
  }
}

export default PrivacyPolicy;
