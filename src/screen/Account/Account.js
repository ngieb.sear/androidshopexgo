import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Share
} from "react-native";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Left,
  Right,
  Body,
  Button,
  Icon,
  Title,
  Thumbnail,
  List,
  ListItem
} from "native-base";

import ServerClass from "../../class/ServerClass";

import { siteUrl, baseUrl } from "../../class/Constant";

import { Navigation } from "react-native-navigation";

import { connect } from "react-redux";

class Account extends Component {
  mounted = false;
  static options() {
    return {
      topBar: {
        background: {
          color: "white"
        },
        elevation: 0
      }
    };
  }

  constructor(props) {
    super(props);
    this.mounted = false;
    this.state = {
      user: 0,
      username: "SIGN IN"
    };
  }

  componentDidMount() {
    this.mounted = true;
    ServerClass.checkUser().then(val => {
      this.setState({ user: val });
      this.responseUsername();
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  componentDidUpdate(prevProps) {
    if (this.props.userID.user_id !== prevProps.userID.user_id) {
      ServerClass.checkUser().then(val => {
        this.setState({ user: val });
        this.responseUsername();
      });
    }
  }

  responseUsername = () => {
    const params = {
      user: this.state.user
    };

    ServerClass.responsePostData(
      siteUrl + "mobileApi/responseUsername",
      params
    ).then(json => {
      if (json.statusCode != "E0001") {
        this.setState({ username: json.username });
      }
    });
  };

  goToScreen = screen => {
    if (this.state.user != 0) {
      Navigation.push(this.props.componentId, {
        component: {
          name: screen,
          passProps: {
            user: this.state.user
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    } else {
      Navigation.push(this.props.componentId, {
        component: {
          name: "JoinOrSignIn",
          passProps: {
            status: "joinSignin"
          },
          options: {
            topBar: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            bottomTabs: {
              visible: false,
              drawBehind: true,
              animate: false
            },
            animations: {
              setStackRoot: {
                enabled: true
              }
            }
          }
        }
      });
    }
  };

  onShare = async () => {
    try {
      const result = await Share.share({
        message: "https://www.loyloy7.com/"
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          ToastAndroid.show("Shared ! ", ToastAndroid.SHORT);
        } else {
        }
      } else if (result.action === Share.dismissedAction) {
        ToastAndroid.show("Canceled ! ", ToastAndroid.SHORT);
      }
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    return (
      <Container style={style.container}>
        {/* <Header noShadow style={{ backgroundColor: "transparent" }}>
          
        </Header> */}

        <Content>
          <Card transparent>
            <CardItem>
              <Left>
                <Title style={{ color: "black", fontSize: 25 }}>
                  {this.state.username}
                </Title>
              </Left>
              {/* <Body>
                
              </Body> */}

              <Right>
                <TouchableOpacity onPress={() => this.goToScreen("Profile")}>
                  <Thumbnail
                    source={require("../../assets/icon/user-account.png")}
                  ></Thumbnail>
                </TouchableOpacity>
              </Right>
            </CardItem>
          </Card>
          <List style={{ marginTop: 20 }}>
            <ListItem icon itemDivider>
              <Left>
                <Button style={{ backgroundColor: "#ff4747" }}>
                  <Icon active name="list-box" />
                </Button>
              </Left>
              <Body>
                <Text style={style.textTitle}>Orders</Text>
              </Body>
            </ListItem>
            <ListItem noBorder onPress={() => this.goToScreen("ViewCart")}>
              <Text style={style.textTitle}>My Ordered</Text>
            </ListItem>
            <ListItem noBorder onPress={() => this.goToScreen("WishList")}>
              <Text style={style.textTitle}>Wish List</Text>
            </ListItem>
            <ListItem noBorder onPress={() => this.goToScreen("Shipped")}>
              <Text style={style.textTitle}>Shipped</Text>
            </ListItem>
            <ListItem noBorder onPress={() => this.goToScreen("Reviewed")}>
              <Text style={style.textTitle}>To be reviewed</Text>
            </ListItem>
            <ListItem itemDivider></ListItem>
            <ListItem
              icon
              noBorder
              onPress={() => this.goToScreen("ShippingAddress")}
            >
              <Left>
                <Button style={{ backgroundColor: "#ff4747" }}>
                  <Icon active name="pin" />
                </Button>
              </Left>
              <Body>
                <Text style={style.textTitle}>Shipping Address</Text>
              </Body>
            </ListItem>
            <ListItem icon noBorder onPress={() => this.onShare()}>
              <Left>
                <Button style={{ backgroundColor: "#ff4747" }}>
                  <Icon active name="share" />
                </Button>
              </Left>
              <Body>
                <Text style={style.textTitle}>Invite Friends</Text>
              </Body>
            </ListItem>

            <ListItem icon noBorder>
              <Left>
                <Button style={{ backgroundColor: "#ff4747" }}>
                  <Icon active name="settings" />
                </Button>
              </Left>
              <Body>
                <Text
                  style={style.textTitle}
                  onPress={() => this.goToScreen("Setting")}
                >
                  Settings
                </Text>
              </Body>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    userID: state
  };
};
export default connect(mapStateToProps)(Account);
// export default Account;

const style = StyleSheet.create({
  container: {
    backgroundColor: "white"
  },
  textTitle: {
    fontSize: 15,
    fontWeight: "bold",
    color: "black"
  }
});
