import React, { Component } from "react";
import { siteUrl } from "../class/Constant";
import AsyncStorage from "@react-native-community/async-storage";

class ServerClass extends Component {
  constructor(props) {
    super(props);
  }

  static async checkUser() {
    try {
      const value = await AsyncStorage.getItem("user_id");
      if (value !== null) {
        return value;
      } else {
        return 0;
      }
    } catch (e) {
      console.log("Error");
    }
  }

  static async responseCountCartItem() {
    try {
      const value = await AsyncStorage.getItem("user_id");
      if (value !== null) {
        const params = {
          user_id: value
        };

        return this.responsePostData(
          siteUrl + "mobileApi/responseCountItem",
          params
        ).then(json => {
          return json.count;
        });
      } else {
        return 0;
      }
    } catch (e) {
      console.log("Error");
    }
  }

  static async setUser(user) {
    try {
      await AsyncStorage.setItem("user_id", user);
    } catch (e) {
      console.log("Error");
    }
  }

  static async removeUser() {
    try {
      await AsyncStorage.removeItem("user_id");
      return true;
    } catch (e) {
      console.log("Error");
    }
  }

  static async responsePostData(url, params) {
    try {
      let respone = await fetch(url, {
        method: "POST",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },

        body: JSON.stringify(params)
      });

      let json = await respone.json();

      return json;
    } catch (error) {
      console.log(error);
    }
  }

  static async responsGetData(url) {
    try {
      let respone = await fetch(url);
      let jsonData = await respone.json();
      return jsonData;
    } catch (error) {
      console.error(`Error is ${error}`);
    }
  }

  static async InsertPostData(url, params) {
    try {
      let respone = await fetch(url, {
        method: "POST",

        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },

        body: JSON.stringify(params)
      });

      let json = await respone.json();
      return json;
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return null;
  }
}

export default ServerClass;
