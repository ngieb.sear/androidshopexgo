import { ADD_TO_CART } from "../Constant";
export const addToCart = id => {
  return {
    type: ADD_TO_CART,
    id
  };
};
