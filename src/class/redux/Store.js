import { createStore, combineReducers } from "redux";

const cartReducer = (state = [], action) => {
  if (action.type === "ADD_TO_CART") {
    return {
      ...state,
      item: {
        id: action.payload.id,
        qty: action.payload.qty,
        price: action.payload.price,
        count: state[action.payload] ? state[action.payload].count + 1 : 1
      }
    };
  }
  return state;
};

const countCartReducer = (state = [], action) => {
  if (action.type === "COUNT_CART") {
    return action.payload.count;
  }
  return state;
};

const setUser = (state = [], action) => {
  if (action.type === "SET_USER") {
    return action.payload.user;
  }
  return state;
};

const combine = combineReducers({
  cartReducer: cartReducer,
  countCartReducer: countCartReducer,
  user_id: setUser
});

const store = createStore(combine);

export default store;
